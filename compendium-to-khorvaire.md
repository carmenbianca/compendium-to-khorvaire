<!--
SPDX-FileCopyrightText: © 2020 Carmen Bianca Bakker <carmen@carmenbianca.eu>
SPDX-FileCopyrightText: 2020 Kristian Serrano
SPDX-FileCopyrightText: 2000-2003, 2016 Wizards of the Coast, Inc.

SPDX-License-Identifier: CC-BY-4.0
-->

\onecolumn

# Credits {-}

**Written & designed by** Carmen Bianca Bakker

**Inspired by**
[_Eberron for Savage Worlds_](https://immaterialplane.com/products/eberron-for-savage-worlds/)
by Kristian Serrano

**Eberron created by** Keith Baker and others

**Savage Worlds written & designed by** Shane Lacy Hensley, with Clint Black

_Wild Cards' Compendium to Khorvaire_ is unofficial Fan Content permitted under
the [Fan Content Policy](https://company.wizards.com/fancontentpolicy). Not
approved/endorsed by Wizards. Portions of the materials used are property of
Wizards of the Coast. ©Wizards of the Coast LLC.

Dungeons & Dragons, D&D, Eberron and its respective logos are trademarks of
Wizards of the Coast in the USA and other countries.

This game references the _Savage Worlds_ game system, available from Pinnacle
Entertainment Group at <https://www.peginc.com>. Savage Worlds and all
associated logos and trademarks are copyrights of Pinnacle Entertainment Group.

Portions of this work are copyright Kristian Serrano, and used with permission
under the condition that the author be attributed. See the hyperlink to _Eberron
for Savage Worlds_ above.

All parts of this work over which the author holds copyright are licensed under
a
[Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).
© 2020 Carmen Bianca Bakker

![](SWFan.jpg){width=300}

\twocolumn

# Foreword

I fell in love with Eberron when I first encountered it in 2019. A noir-pulpy
world of international politics, magical technology, cyberpunky oligarchical
corporations, ancient dangers, airships, LARPing elves, and dinosaur-riding
halflings? Sign me up! I love that Eberron makes the mundane so utterly
fascinating. I am enthralled by the existence of its magical economy and
magic-as-modern-technology. I love that its monsters aren't objective evils to
be vanquished, but a political entity of their own. I like that gods probably
don't exist, allowing a world in which the villains can be morally grey, and the
gritty reality that the heroes don't always win. And although _Dungeons &
Dragons_ is an excellent game, I struggled a lot with translating many of these
mundane concepts into actual play as soon as the players reached its second tier
of play.

So when I discovered _Savage Worlds_ through Kristian Serrano on
[Manifest Zone](https://manifest.zone/), I fell in love with it like I fell in
love with Eberron. It addressed the issues I had with running Eberron campaigns
using D&D, and then some. I also discovered his
[_Eberron for Savage Worlds_](https://immaterialplane.com/products/eberron-for-savage-worlds/),
and greatly enjoyed thumbing through it, but it wasn't a precise fit for me.

_Wild Cards' Compendium to Khorvaire_, therefore, is my spin on this thing.
Specifically, I am trying to achieve the following things:

- Depend exclusively on the core rules in SWADE and the SWADE Fantasy Companion.
- Create a rules-light setting. By including as few additional mandatory rules
  as possible, GMs can more easily adopt and adapt this document. SWADE is an
  excellent foundation that does not need much additional tweaking. For extra
  stuff, see
  [_Wild Cards' Expanded Compendium to Khorvaire_](https://carmenbianca.gitlab.io/compendium-to-khorvaire/expanded/).
- Do not include cultural traits in the races. For instance, the All Thumbs
  Hindrance might make sense for the elven culture, but not for the elven body.
  Conflation of species/race and culture is fairly inherent to _Dungeons &
  Dragons_, but I want to break with that trope. For some races this is
  especially difficult. I'm looking at you, hobgoblins.
- Facilitate wandslingers.
- Do not include too much flavour text. The _Eberron Campaign Setting_ and
  _Eberron: Rising From the Last War_ are excellent lore resources that this
  document should not attempt to replicate.

Whether or not I succeed at these things is ultimately for the reader to decide.
I am not a professional designer or writer, but I do my best.

Special thanks to Kristian Serrano for his
[_Eberron for Savage Worlds_](https://immaterialplane.com/products/eberron-for-savage-worlds/).
Not-insignificant portions of this document are borrowed from or inspired by his
work. I highly recommend to check it out---Kristian Serrano is doubtlessly a
more experienced writer than I am, and a lot more eyeballs have gone over his
work.

With kindness,  
Carmen

## Namespace

The name of this document is _Wild Cards' Compendium to Khorvaire_, which sounds
a lot fancier than it ought to. The reason I chose the name is to avoid a
namespace collision with the much more sensibly named _Eberron for Savage
Worlds_.

## Source and sharing

The source code for this document is available at
<https://gitlab.com/carmenbianca/compendium-to-khorvaire>. It is written in a
single Markdown file, and should be really easy to fork and change. Pandoc is
used to generate all file formats.

You can find hosted versions of this document at:

- HTML --- <https://carmenbianca.gitlab.io/compendium-to-khorvaire/>
- EPUB ---
  [https://carmenbianca.gitlab.io/compendium-to-khorvaire/Wild Cards' Compendium to Khorvaire.epub](https://carmenbianca.gitlab.io/compendium-to-khorvaire/Wild%20Cards%27%20Compendium%20to%20Khorvaire.epub)
- PDF ---
  [https://carmenbianca.gitlab.io/compendium-to-khorvaire/Wild Cards' Compendium to Khorvaire.pdf](https://carmenbianca.gitlab.io/compendium-to-khorvaire/Wild%20Cards%27%20Compendium%20to%20Khorvaire.pdf)

The _Wild Cards' Expanded Compendium to Khorvaire_ can be found at:

- HTML --- <https://carmenbianca.gitlab.io/compendium-to-khorvaire/expanded/>
- EPUB ---
  [https://carmenbianca.gitlab.io/compendium-to-khorvaire/Wild Cards' Expanded Compendium to Khorvaire.epub](https://carmenbianca.gitlab.io/compendium-to-khorvaire/Wild%20Cards%27%20Expanded%20Compendium%20to%20Khorvaire.epub)
- PDF ---
  [https://carmenbianca.gitlab.io/compendium-to-khorvaire/Wild Cards' Expanded Compendium to Khorvaire.pdf](https://carmenbianca.gitlab.io/compendium-to-khorvaire/Wild%20Cards%27%20Expanded%20Compendium%20to%20Khorvaire.pdf)

As stated in the credits, all parts of this document over which I have rights
are released under an attribution licence. If you wish to create a derivative
version of this document, you are more than free to. In fact, you are encouraged
to. While I love and appreciate feedback, one document can never possibly
account for everybody's tastes and preferences.

When you do create a derivative version, you must at a minimum attribute all
authors of this document, preferably in the relevant section at the top. Also,
please be mindful about not including material for which you have no permissions
from the copyright holders.

# Archetypes

Sometimes you want to quickly create a character or gather inspiration for a
certain type of character. The list of archetypes below might help you.

None of the archetypes have any racial bonuses. Simply choose a race afterwards
and add their bonuses, then choose your gear to finish the character.

Alternatively, the GM can use these archetypes as a bestiary for opponents and
allies.

<!--
**Attributes:** Agility d4, Smarts d4, Spirit d4, Strength d4, Vigor d4
**Skills:** Athletics d4, Common Knowledge d4, Notice d4, Persuasion d4, Stealth
d4
**Pace:** 6; **Parry:** TODO; **Toughness:** TODO
**Hindrances:** One Major, two Minor
**Edges:** TODO
**Powers:** TODO
-->

## Aristocrat

**Attributes:** Agility d4, Smarts d10, Spirit d8, Strength d4, Vigor d4  
**Skills:** Academics d8, Athletics d4, Common Knowledge d6, Gambling d4, Notice
d8, Persuasion d8, Research d4, Riding d4, Stealth d4, Taunt d4  
**Pace:** 6; **Parry:** 2; **Toughness:** 4  
**Hindrances:** One Major, two Minor  
**Edges:** Aristocrat, Connections

## Artificer

**Attributes:** Agility d6, Smarts d8, Spirit d6, Strength d4, Vigor d6  
**Skills:** Athletics d4, Common Knowledge d6, Fighting d4, Notice d6,
Persuasion d4, Repair d8, Research d6, Science d8, Shooting d6, Stealth d4  
**Pace:** 6; **Parry:** 4; **Toughness:** 5  
**Hindrances:** One Major, two Minor  
**Edges:** Arcane Background (Artificer), Mr. Fix It  
**Powers:** _Bolt_, _lock/unlock_; **Power Points:** 15

## Barbarian

**Attributes:** Agility d8, Smarts d4, Spirit d6, Strength d8, Vigor d6  
**Skills:** Athletics d8, Common Knowledge d4, Fighting d10, Intimidation d6,
Notice d4, Persuasion d4, Riding d6, Stealth d4, Taunt d4  
**Pace:** 6; **Parry:** 7; **Toughness:** 5  
**Hindrances:** One Major, two Minor  
**Edges:** Berserk

## Bard (Magical)

**Attributes:** Agility d6, Smarts d6, Spirit d8, Strength d4, Vigor d6  
**Skills:** Academics d6, Athletics d4, Common Knowledge d6, Intimidation d6,
Notice d4, Performance d8, Persuasion d8, Stealth d4, Taunt d6  
**Pace:** 6; **Parry:** 2; **Toughness:** 5  
**Hindrances:** One Major, two Minor  
**Edges:** Arcane Background (Bard), Charismatic  
**Powers:** _Boost/lower trait_, _sound/silence_, _empathy_; **Power Points:**
10

## Bard (Non-magical)

**Attributes:** Agility d6, Smarts d6, Spirit d8, Strength d4, Vigor d6  
**Skills:** Academics d6, Athletics d4, Common Knowledge d6, Intimidation d6,
Notice d4, Performance d8, Persuasion d8, Stealth d4, Taunt d6  
**Pace:** 6; **Parry:** 2; **Toughness:** 5  
**Hindrances:** One Major, two Minor  
**Edges:** Charismatic, Reliable

## Beast Master

**Attributes:** Agility d6, Smarts d6, Spirit d8, Strength d4, Vigor d6  
**Skills:** Athletics d4, Common Knowledge d4, Healing d6, Notice d6, Persuasion
d8, Riding d6, Shooting d6, Stealth d6, Survival d6  
**Pace:** 6; **Parry:** 2; **Toughness:** 5  
**Hindrances:** One Major, two Minor  
**Edges:** Beast Bond, Beast Master

## Cleric

**Attributes:** Agility d6, Smarts d6, Spirit d8, Strength d6, Vigor d6  
**Skills:** Academics d6, Athletics d4, Common Knowledge d6, Cosmology d4, Faith
d8, Fighting d6, Healing d4, Notice d6, Persuasion d6, Stealth d4  
**Pace:** 6; **Parry:** 5; **Toughness:** 5  
**Hindrances:** One Major, two Minor  
**Edges:** Arcane Background (Cleric)  
**Powers:** _Healing_, _light/darkness_, _relief_, _sanctuary_, _smite_; **Power
Points:** 10

## Druid

**Attributes:** Agility d6, Smarts d6, Spirit d8, Strength d4, Vigor d6  
**Skills:** Athletics d4, Common Knowledge d6, Cosmology d4, Faith d8, Fighting
d4, Healing d4, Notice d6, Persuasion d6, Riding d4, Stealth d4, Survival d6  
**Pace:** 6; **Parry:** 4; **Toughness:** 5  
**Hindrances:** One Major, two Minor  
**Edges:** Arcane Background (Druid), Beast Master  
**Powers:** _Beast friend_, _environmental protection_, _havoc_, _shape change_,
_summon animal_; **Power Points:** 10

## Fighter (Melee)

**Attributes:** Agility d8, Smarts d6, Spirit d6, Strength d8, Vigor d6  
**Skills:** Athletics d8, Battle d4, Common Knowledge d4, Fighting d10,
Intimidation d4, Notice d4, Persuasion d4, Riding d4, Stealth d6, Taunt d4  
**Pace:** 6; **Parry:** 7; **Toughness:** 5  
**Hindrances:** One Major, two Minor  
**Edges:** ---

## Fighter (Ranged) / Wandslinger

**Attributes:** Agility d10, Smarts d6, Spirit d6, Strength d4, Vigor d6  
**Skills:** Athletics d8, Battle d4, Common Knowledge d4, Fighting d6, Notice
d6, Persuasion d4, Riding d4, Shooting d10, Stealth d6  
**Pace:** 6; **Parry:** 5; **Toughness:** 5  
**Hindrances:** One Major, two Minor  
**Edges:** Trademark Weapon

## Inquisitive

**Attributes:** Agility d6, Smarts d10, Spirit d6, Strength d4, Vigor d4  
**Skills:** Academics d6, Athletics d4, Common Knowledge d6, Notice d8,
Persuasion d6, Research d8, Stealth d6, Thievery d6  
**Pace:** 6; **Parry:** 2; **Toughness:** 4  
**Hindrances:** One Major, two Minor  
**Edges:** Alertness, Investigator

## Monk

**Attributes:** Agility d8, Smarts d6, Spirit d6, Strength d6, Vigor d6  
**Skills:** Athletics d8, Common Knowledge d4, Fighting d10, Notice d6,
Persuasion d4, Research d4, Stealth d6, Taunt d6  
**Pace:** 6; **Parry:** 7; **Toughness:** 5  
**Hindrances:** One Major, two Minor  
**Edges:** Martial Artist

## Paladin

**Attributes:** Agility d6, Smarts d4, Spirit d6, Strength d8, Vigor d6  
**Skills:** Academics d4, Athletics d6, Common Knowledge d4, Faith d6, Fighting
d8, Healing d4, Notice d4, Persuasion d6, Riding d6, Stealth d4  
**Pace:** 6; **Parry:** 6; **Toughness:** 5  
**Hindrances:** One Major, two Minor  
**Edges:** Arcane Background (Cleric), Champion  
**Powers:** _Boost trait_, _healing_, _light_, _sanctuary_, _smite_; **Power
Points:** 10

## Psion

**Attributes:** Agility d6, Smarts d8, Spirit d6, Strength d4, Vigor d6  
**Skills:** Academics d6, Athletics d4, Common Knowledge d6, Cosmology d6,
Fighting d4, Notice d6, Persuasion d6, Psionics d8, Stealth d6  
**Pace:** 6; **Parry:** 4; **Toughness:** 5  
**Hindrances:** One Major, two Minor  
**Edges:** Arcane Background (Psionics), Charismatic  
**Powers:** _Confusion_, _mind link_, _mind reading_; **Power Points:** 10

## Ranger

**Attributes:** Agility d8, Smarts d6, Spirit d6, Strength d6, Vigor d6  
**Skills:** Athletics d6, Common Knowledge d4, Fighting d6, Notice d6,
Persuasion d4, Shooting d8, Stealth d6, Survival d8  
**Pace:** 6; **Parry:** 5; **Toughness:** 5  
**Hindrances:** One Major, two Minor  
**Edges:** Woodsman

## Rogue

**Attributes:** Agility d8, Smarts d6, Spirit d6, Strength d6, Vigor d4  
**Skills:** Athletics d8, Common Knowledge d4, Fighting d6, Gambling d4, Notice
d6, Persuasion d4, Shooting d4, Stealth d8, Thievery d8  
**Pace:** 6; **Parry:** 5; **Toughness:** 4  
**Hindrances:** One Major, two Minor  
**Edges:** Assassin, Thief

## Sorcerer

**Attributes:** Agility d4, Smarts d6, Spirit d10, Strength d4, Vigor d8  
**Skills:** Athletics d4, Common Knowledge d6, Cosmology d6, Fighting d4, Focus
d10, Notice d6, Persuasion d6, Research d4, Shooting d4, Stealth d4  
**Pace:** 6; **Parry:** 4; **Toughness:** 6  
**Hindrances:** One Major, two Minor  
**Edges:** Arcane Background (Sorcerer)  
**Powers:** _Burst_, _dispel_, _havoc_; **Power Points:** 15

## Warlock

**Attributes:** Agility d4, Smarts d8, Spirit d8, Strength d4, Vigor d6  
**Skills:** Academics d6, Athletics d4, Common Knowledge d6, Cosmology d6,
Fighting d4, Notice d6, Persuasion d8, Spellcasting d8, Stealth d4  
**Pace:** 6; **Parry:** 4; **Toughness:** 5  
**Hindrances:** One Major, two Minor  
**Edges:** Arcane Background (Warlock/Witch), New Powers  
**Powers:** _Bolt_, _fear_, _light/darkness_; **Power Points:** 10

## Wizard

**Attributes:** Agility d6, Smarts d10, Spirit d6, Strength d4, Vigor d4  
**Skills:** Athletics d4, Common Knowledge d6, Notice d4, Persuasion d4,
Research d8, Science d6, Shooting d6, Spellcasting d10, Stealth d4  
**Pace:** 6; **Parry:** 2; **Toughness:** 4  
**Hindrances:** One Major, two Minor  
**Edges:** Arcane Background (Wizard), Scholar  
**Powers:** _Arcane protection_, _bolt_, _detect/conceal arcana_, _dispel_,
_illusion_, _lock/unlock_; **Power Points:** 15

# Races

The below races are examples of ordinary physiology. A character may differ from
that for any reason. Features that are considered especially malleable are
marked with an asterisk (\*) for your convenience. Each racial feature has
positive or negative points associated with it. The function of these points can
be found in **Making Races** in _Savage Worlds_. The below races are balanced to
have 4 points.

If a racial feature does not work for your character, you can remove it, and
acquire that feature's points, then find an appropriate replacement.

If you want to adjust your character's Size, you can do that without tweaking
your character's race, using the Small Hindrance, the Obese Hindrance, or the
Brawny Edge.

## Bugbears

<!--
Purely physiological traits copied from 5e. Also stealth.
-->

- **Big (-2):** Bugbears subtract two when using equipment designed for smaller
  beings, and cannot wear humanoid armor or clothing. Equipment, armor, food and
  clothing cost double the listed price.
- **Darkvision (+1):** Bugbears ignore penalties for Illumination up to 10" (20
  yards).
- **Reach (+1):** Bugbears have long arms. This grants them Reach +1.
- **Size +1 (+1):** Bugbears are tall creatures, increasing their Size (and
  therefore Toughness) by 1.
- **Sneaky (+1\*)**: Bugbears start with a d6 in Stealth instead of a d4. This
  increases maximum Stealth to d12 + 1.
- **Powerful Build (+2\*):** Bugbears start with a d6 in Strength instead of a
  d4. This increases maximum Strength to d12 + 1.

## Changelings

<!-- As simple as can be. -->

- **Change Appearance (+3):** Changelings can alter physical appearance as a
  limited free action. They can assume the appearance of another person of the
  same Size and shape, although clothing and gear is not affected. The effect is
  non-magical and cannot be detected. If unconscious or dead, they return to
  their natural form.
- **Changeling Instincts (+1\*):** Changelings start with a d6 in Persuasion
  instead of a d4. This increases maximum Persuasion to d12 + 1.

## Drow

<!-- Almost identical to elves. -->

- **Agile (+2\*):** Drow start with a d6 in Agility instead of a d4. This
  increases maximum Agility to d12 + 1.
- **Fey Ancestry (+2):** Drow start with the Fey Blood Edge.
- **Frail (-1\*):** Drow decrease their Toughness by 1.
- **Keen Senses (+1\*):** Drow start with a d6 in Notice instead of a d4. This
  increases maximum Notice to d12 + 1.
- **Sunlight Sensitivity (-2):** Drow are Distracted in sunlight.
- **Darkvision (+1):** Drow ignore penalties for Illumination up to 10" (20
  yards).
- **Trance (+1):** Drow don't need to sleep. Instead, they meditate deeply,
  remaining semi-conscious. This trance lasts approximately three hours.

## Dwarves

<!-- Turns out that dwarves have very few unique traits. -->

- **Darkvision (+1):** Dwarves ignore penalties for Illumination up to 10" (20
  yards).
- **Dwarven Resilience (+1):** Dwarves receive a +4 bonus to resist poison.
  Damage from poison is also reduced by 4.
- **Reduced Pace (-1):** Decrease the character's Pace by 1 and their running
  die one die type
- **Tough (+3\*):** Dwarves start with a d6 in Vigor instead of a d4. This
  increases maximum Vigor to d12 + 1. They treat their Strength as one die type
  higher when determining **Encumbrance** (see _Savage Worlds_) and Minimum
  Strength to use armor and equipment without penalty (but not weapons).

## Elves

<!--
Only the unique physiological traits from 5e. Keen Senses reflect their good
hearing and eyesight.
-->

- **Agile (+2\*):** Elves start with a d6 in Agility instead of a d4. This
  increases maximum Agility to d12 + 1.
- **Fey Ancestry (+2):** Elves start with the Fey Blood Edge.
- **Frail (-3\*):** Elves decrease their Toughness by 1 and suffer a -1 penalty
  to Vigor rolls.
- **Keen Senses (+1\*):** Elves start with a d6 in Notice instead of a d4. This
  increases maximum Notice to d12 + 1.
- **Low Light Vision (+1):** Elves ignore penalties for Dim and Dark
  Illumination.
- **Trance (+1):** Elves don't need to sleep. Instead, they meditate deeply,
  remaining semi-conscious. This trance lasts approximately three hours.

## Gnomes

<!-- Gnome Cunning is a weird physiological trait. -->

- **Gnome Cunning (+2):** Gnomes start with the Fey Blood Edge.
- **Intelligent (+2\*):** Gnomes start with a d6 in Smarts instead of a d4. This
  increases maximum Smarts to d12 + 1.
- **Keen Senses (+1\*):** Gnomes start with a d6 in Notice instead of a d4. This
  increases maximum Notice to d12 + 1.
- **Low Light Vision (+1):** Gnomes ignore penalties for Dim and Dark
  Illumination.
- **Reduced Pace (-1):** Decrease the character's Pace by 1 and their running
  die one die type.
- **Size -1 (-1):** Gnomes average between three and four feet tall, reducing
  their Size (and therefore Toughness) by 1.

## Goblins

<!-- Goblins are small and have darkvision. -->

- **Agile (+2\*):** Goblins start with a d6 in Agility instead of a d4. This
  increases maximum Agility to d12 + 1.
- **Elusive (+1\*):** Goblins start with a d6 in Stealth instead of a d4. This
  increases maximum Stealth to d12 + 1.
- **Darkvision (+1):** Goblins ignore penalties for Illumination up to 10" (20
  yards).
- **Nimble (+1\*):** Goblins start with a d4 in Thievery.
- **Size -1 (-1):** Goblins stand three to four feet tall, reducing their Size
  (and therefore Toughness) by 1.

## Half-elves (Khoravar)

<!--
Fey Ancestry and Low Light Vision from elves. Flexibility.
-->

- **Fey Ancestry (+2):** Half-elves start with the Fey Blood Edge.
- **Low Light Vision (+1):** Half-elves ignore penalties for Dim and Dark
  Illumination.
- **Flexibility (+1):** A half-elf may start with a d6 in one Attribute instead
  of a d4. This does not increase the Attribute's maximum.

## Half-orcs

<!--
Darkvision and Powerful Build from orcs. Flexibility.
-->

- **Darkvision (+1):** Half-orcs ignore penalties for Illumination up to 10" (20
  yards).
- **Powerful Build (+2\*):** Half-orcs start with a d6 in Strength instead of a
  d4. This increases maximum Strength to d12 + 1.
- **Flexibility (+1):** A half-orc may start with a d6 in one Attribute instead
  of a d4. This does not increase the Attribute's maximum.

## Halflings

<!-- Luck is a weird trait. -->

- **Brave (+2\*):** Halflings start with the Brave Edge.
- **Luck (+2\*):** Halflings start with the Luck Edge.
- **Reduced Pace (-1):** Decrease the character's Pace by 1 and their running
  die one die type.
- **Size -1 (-1):** Halflings average only about three feet tall, reducing their
  Size (and therefore Toughness) by 1.
- **Spirited (+2\*):** Halflings start with a d6 in Spirit instead of a d4. This
  increases maximum Spirit to d12 + 1.

## Hobgoblins

<!-- I hate this race. -->

- **Darkvision (+1):** Hobgoblins ignore penalties for Illumination up to 10"
  (20 yards).
- **Jingoistic (-1\*):** Hobgoblins have the Jingoistic (Minor) Hindrance.
- **Spirited (+2\*):** Hobgoblins start with a d6 in Spirit instead of a d4.
  This increases maximum Spirit to d12 + 1.
- **Unshakeable (+2\*):** Hobgoblins do not easily flinch. They begin play with
  the Combat Reflexes or the Command Edge---their choice.

## Humans

<!-- Identical to core SW, added attribute. -->

- **Adaptable (+4):** Humans begin play with any Novice Edge of their choosing.
  They must meet its Requirements as usual. They also start with a d6 in an
  attribute instead of a d4. The attribute's maximum increases to d12 + 1.

## Kalashtar

<!-- Fairly simple. -->

- **Dual Mind (+3):** Kalashtar get a free reroll when rolling to resist hostile
  psionic powers and effects. They also gain the Combat Reflexes Edge.
- **Hunted (-2):** Kalashtar are hunted by the Dreaming Dark and the Inspired of
  Riedra. They have the Enemy (Major) Hindrance.
- **Psionic (+3):** Kalashtar have the Arcane Background (Psionics) Edge and
  begin with the _mind link_ and _mind reading_ powers, and one power of their
  choice. Additionally, they have a d4 in Psionics.
- **Severed from Dreams (+0):** Kalashtar sleep, but do not connect to the plane
  of dreams. Instead, they draw on the memories of the quori spirit to which
  they are connected.

## Lizardfolk

<!-- Fairly simple. -->

- **Armor +2 (+1):** Lizardfolk have tough, scaly skin, granting them Armor +2.
- **Bite (+2):** A lizardfolk's fanged maw is a **Natural Weapon** (see _Savage
  Worlds_) that causes Strength+d6 damage.
- **Environmental Weakness (Cold) (-1):** Lizardfolk suffer a -4 penalty to
  resist the cold, and suffer +4 damage from cold-based attacks.
- **Semi-Aquatic (+1):** Lizardfolk can hold their breath for 15 minutes. They
  move their full speed when swimming.

## Orcs

<!-- Tall, strong, and darkvision. -->

- **Darkvision (+1):** Orcs ignore penalties for Illumination up to 10" (20
  yards).
- **Powerful Build (+2\*):** Orcs start with a d6 in Strength instead of a d4.
  This increases maximum Strength to d12 + 1.
- **Size +1 (+1):** Orcs are hulking creatures, increasing their Size (and
  therefore Toughness) by 1.

## Shifters

<!--
The 5e shifter is rather unexciting, and this implementation ticks all the
boxes. Natural Weapon is from the shifter stat block.

Shifting mechanic inspired by Serrano.
-->

- **Low Light Vision (+1):** Shifters ignore penalties for Dim and Dark
  Illumination.
- **Natural Weapon (+1):** Shifters have _claws_, _fangs_, or _horns_ that cause
  Strength+d4 damage. See **Natural Weapons** in _Savage Worlds_.
- **Shifting (+2):** Shifters can tap into their lycanthropic heritage. Shifting
  is a free action and lasts 10 rounds or until dismissed as a free action.
  While in this animal-like form, the shifter can ignore 1 Wound penalty. When
  the shifting ends, the shifter must wait 4 hours before shifting again or
  suffer Fatigue. The type of shifting is determined by their subrace.
  - **Beasthide:** The skin of a beasthide shifter hardens, increasing their
    Toughness by +2.
  - **Longtooth:** The Natural Weapons of the longtooth shifter grow larger,
    increasing their damage die by a die type. Additionally, they add +4 to
    damage when making a Wild Attack with their Natural Weapons instead of the
    usual +2.
  - **Swiftstride:** The legs of a swiftstride shifter morph. Their Pace is
    increased by +2 and their running die is increased by a die type.
    Additionally, the shifter may ignore the -2 penalty from running.
  - **Wildhunt:** The senses of a wildhunt shifter become superhuman. The
    shifter gets a free reroll on Notice and Survival rolls. Additionally, the
    shifter has a +2 bonus for any Trait roll made to track a creature.

## Warforged

<!--
Tried to use Construct from core rules. Caveat is that healing spells *do* work
on warforged. Construct rules made no mention of sleeping, so added that here.

Important to note: This warforged reflects the 3.5e race more than the 5e race.
I think that's okay.

Integrated Protection is really really strong. But it makes a lot of sense for
warforged, because they are literal beings of armor.
-->

- **Bulky (-2):** Warforged subtract two when using equipment designed for
  smaller beings, and cannot wear humanoid armor or clothing. Equipment, armor,
  food and clothing cost double the listed price.
- **Factory Settings (-2\*):** Warforged were designed, built, and trained for a
  single purpose. Their default knowledge and abilities outside of that purpose
  are limited. They do not have Common Knowledge and Persuasion as core skills.
- **Integrated Protection (+1):** A warforged is covered in tough plating made
  of magically-treated woods and metals. Warforged gain Armor +4. Unlike other
  natural Armor, this natural Armor counts as one layer (see **Armor** in
  _Savage Worlds_). The natural Armor may be improved, replaced, or even
  removed---GM's call.
- **Living Construct (+8):** Warforged add +2 to recover from being Shaken,
  ignore one level of Wound modifiers, do not need to breathe, eat, or drink,
  and are immune to poison and disease. Instead of sleeping, warforged recharge
  in an inactive, motionless, but conscious state. Warforged cannot heal
  naturally. They are healed using the Repair skill, which takes one hour per
  current Wound level per attempt and is not limited to the "**Golden Hour**".
  Alternatively, magical healing can heal a warforged.
- **Quirk (-1\*):** Warforged often display an odd personality trait or two.
  They have the Quirk Hindrance.

# Hindrances

## Prohibited Hindrances

### Outsider

The Outsider Hindrance is removed. In its place, the
[**Tolerance and Prejudice**](#tolerance-and-prejudice) Setting Rule applies to
everyone.

# Skills

## Removed Skills

The following skills are not used in _Wild Cards' Compendium to Khorvaire_:
Electronics, Hacking, and Occult.

## Modified Skills

### Science (Smarts)

Specialized knowledge of magic is part of the Science skill in Eberron. This
substitutes Occult.

## New Skills

### Cosmology (Smarts)

Eberron is surrounded by many other planes. In the night sky, the ring of
Siberys is always visible, and twelve moons dot the sky. Beneath the surface,
Khyber lurks. Outside of the material plane, a dozen other planes are connected
to Eberron through manifest zones. Characters roll Cosmology to know properties
of these things outside of Eberron.

# Edges

## Prohibited Edges

The following Edges are prohibited in _Wild Cards' Compendium to Khorvaire_:
Double Tap, McGyver, Rapid Fire, Rock and Roll!, Two-Gun Kid.

## New Edges

### Dragonmark

**Requirements:** Novice, no other dragonmark

The character has a dragonmark appropriate to their race. It grants the ability
to use dragonmark focus items and a bonus to specific skills. See the
Dragonmarks table.

Additionally, they gain access to 10 dedicated Power Points using the rules of
the Mystic Powers Edge (_Savage Worlds Fantasy Companion_). Their package
depends on their specific mark. Unlike with the Mystic Powers Edge, dragonmarked
characters can take an Arcane Background and cast their dragonmark's powers
using the background's Power Points.

The Epic Mastery Edge may be used with Dragonmark.

\onecolumn

<!-- prettier-ignore-start -->

| Mark        |      Race       | Skill Bonus | Package |
| :-- | :--: | :-------- | :--------- |
| Detection   |    Half-elf     | +1 to Notice rolls | _Detect/conceal arcana_ (_detect_ has the Self Limitation), _mind reading_, _object reading_, _scrying_. |
| Finding     | Half-orc, human | +1 to Survival rolls | _Darksight_, _farsight_, _locate_, _scrying_. All have the Self Limitation. |
| Handling    |      Human      | +1 to Riding and Persuasion rolls when dealing with animals | _Beast friend_, _boost trait_ (animals only), _relief_ (animals only), _summon animal_. |
| Healing     |    Halfling     | +1 to Healing rolls | _Boost trait_ (Vigor only), _healing_ (PP cost of Greater Healing and Crippling Injuries reduced by 5), _relief_. |
| Hospitality |    Halfling     | +1 to Persuasion rolls when dealing with creatures of human intelligence | _Conjure item_ (food and water only), _empathy_, _relief_, _slumber_. |
| Making      |      Human      | +1 to Repair rolls | _Conjure item_, _detect/conceal arcana_ (_detect_ has the Self Limitation), _dispel_ (targeting items only), _smite_. |
| Passage     |      Human      | +1 to Driving and Riding rolls | _Speed_ (Self Limitation), _telekinesis_, _teleport_, _wall walker_. |
| Scribing    |      Gnome      | +1 to Research rolls | _Arcane mark_, _mind link_ (Long Distance Link immediately available), _speak language_ (Self Limitation). |
| Sentinel    |      Human      | +1 to Athletics, Fighting, and Shooting rolls related to combat | _Deflection_, _protection_, _sanctuary_, _warrior's gift_ (Self Limitation). |
| Shadow      |       Elf       | +1 to Performance and Stealth rolls | _Darkness_, _disguise_, _illusion_, _sound/silence_. |
| Storm       |    Half-elf     | +1 to Boating and Piloting rolls | _Elemental manipulation_ (air and water only), _environmental protection_ (air, storm, and water only), _havoc_, _stun_. |
| Warding     |      Dwarf      | +1 to Thievery rolls related to locks and traps | _Barrier_, _lock/unlock_, _protection_, _ward_. |

Table: Dragonmarks

<!-- prettier-ignore-end -->

\twocolumn

### Improved Aberrant Dragonmark

**Requirements:** Seasoned,
[Arcane Background (Aberrant Dragonmark)](#arcane-background-aberrant-dragonmark)

The bearer of an aberrant dragonmark ignores an additional 2 points of shorting
penalties to Focus rolls when casting powers granted by their mark.

Improved Aberrant Dragonmark may be selected more than once, but only once per
Rank. Each time the character selects this Edge, the dragonmark changes into a
larger, more complex pattern and increases in power.

### Improved Integrated Armor

**Requirements:** Novice, Warforged

The warforged underwent a procedure to reinforce their armor. They gain another
Armor +2.

# Gear

## Simplified Currency

To maintain compatibility with other _Savage Worlds_ resources, the currency
system in _Wild Cards' Compendium to Khorvaire_ is simplified compared to
standard Eberron. While Eberron has copper crowns, silver sovereigns, gold
galifars, and platinum dragons, _Wild Cards' Compendium to Khorvaire_'s currency
is exclusively expressed in sovereigns.

If you want to, you may use the other three currencies as well, but it is
generally easier to keep track of a single currency.

Prices from official Eberron resources, when converted to silver sovereigns, may
not make sense in _Wild Cards' Compendium to Khorvaire_. Some discretion on the
part of the GM is required here.

An advice for GMs who stress out over prices is to simply peg the silver
sovereign to your local real-world currency. If a loaf of bread costs one Euro
where you live, it costs one sovereign in Eberron. All other prices should then
be ballparked along those lines.

## New Melee Weapons

**Scimitar, Double-Bladed:** The double-bladed scimitar is the signature weapon
of Valenar elves.

## New Ranged Weapons

**Boomerang:** Boomerangs, commonly found among the halflings of the Talenta
plains, are curved, polished sticks designed to return to the thrower. On a
critical failure, the boomerang deviates from the thrower, including behind the
thrower (see **Deviation** under **Area Effect Attacks** in _Savage Worlds_).

\onecolumn

| Type                    |  Damage  | Min Str. | Weight | Cost  | Notes     |
| :---------------------- | :------: | :------: | :----: | :---: | :-------- |
| Boomerang               |  Str+d4  |    d4    |   1    |  50   | ---       |
| Scimitar, Double-Bladed | Str+d6+1 |    d6    |   4    | 1,000 | Two hands |

Table: Melee Weapons

| Type      |  Range  | Damage | AP  | ROF | Min Str. | Weight | Cost |
| :-------- | :-----: | :----: | :-: | :-: | :------: | :----: | :--: |
| Boomerang | 6/12/24 | Str+d4 | --- |  1  |    d4    |   1    |  50  |

Table: Ranged Weapons

\twocolumn

## Miscellaneous

**Dragonshard, Eberron:** Eberron dragonshards can be used to fuel almost any
magic. They contain Power Points dependent on the size of the shard. Anyone
carrying one in hand can use its Power Points as an action (incurring a
Multi-Action penalty) to cast a spell or recharge a wand. The dragonshard does
not recharge.

- **Price:** 10 per Power Point; **Weight:** 1 lb per 10 Power Points.

**Everbright Lantern:** A lantern containing an Eberron dragonshard that sheds
light in a 4" radius (Dim Light up to twice that), or a Cone Template if the
lantern focuses the light in a single direction. The dragonshard never stops
shedding light, but one can screen the lantern.

- **Price:** 500; **Weight:** 3 lb.

**Feather Token:** A small metal disk engraved with the symbol of a feather.
When the bearer falls 3" while the token is on their person, their rate of
descent is slowed to 10" per round and they take no damage from falling. The
feather token's magic is then expended.

- **Price:** 50; **Weight:** ---

**Wand of Bolts:** This wand grants the wielder _Bolt_ with the buyer's choice
of one common Power Modifier, uses Shooting or the user's arcane skill (their
choice), and has 25 Power Points. These wands come in lots of different
Trappings. Additional Power Modifiers increase the price. Skilled magewrights
are able to recharge these wands at a fee of 1 sovereign per Power Point.

- **Price:** 250; **Weight:** 1 lb.

# Setting Rules

## Multiple Languages

Eberron features characters and cultures who speak different languages. Use the
**Multiple Languages** setting rule from **Setting Rules** in _Savage Worlds_.

## Tolerance and Prejudice

This Setting Rule replaces the removed [**Outsider**](#outsider) Hindrance. A
character subtracts 2 from Persuasion rolls made to influence those who are
prejudiced against their kind.

Make sure to clear up with your players how much they want or don't want
prejudice---and what kinds of prejudice---to be a part of the game. Although
it's understood that there exists bad blood between some nationalities in
Eberron, you don't have to play it up if you don't want to.

> #### Rationale
>
> The Outsider Hindrance doesn't make a whole heap of sense. It applies a
> blanket penalty to interactions with everyone who doesn't share a certain
> characteristic with you. This isn't how the world works. There are heaps of
> people---especially in a diverse world such as Eberron---who simply aren't
> prejudiced.
>
> The Outsider Hindrance forces discrimination into almost every social
> encounter, which may not be what the people at the table want, and may not be
> conducive to the fiction. This Setting Rule dials that back.

# Powers

## Arcane Backgrounds

### Arcane Background (Aberrant Dragonmark)

Your character has manifested an aberrant dragonmark. Aberrant dragonmarks are
unpredictable, and no two such marks are ever alike.

- **Requirements:** No other dragonmark
- **Arcane Skill:** Focus (Spirit)
- **Starting Powers:** 1
- **Power Points:** ---
- **Available Powers:** Any.
- **Unpredictable Magic:** Aberrants can cast powers by shorting them. They
  ignore 2 points of shorting penalties, and can use bennies to re-roll failed
  arcane skill rolls. If the power does damage, they roll an extra d6 and add it
  to the total.
- **No Power Points:** For this Arcane Background, characters cannot take
  additional Edges which grant or require power points, such as Power Points or
  Rapid Recharge.

> #### A Note on Balance
>
> This Arcane Background is not designed to be balanced in play. Furthermore, it
> does not reflect the true diversity of the aberrant dragonmark. One aberrant
> dragonmark might attack a victim with their own worst fears; another
> perpetually absorbs all sound in the immediate vicinity; and another regrows
> miscellaneous body parts when they are wounded. Some of these abilities map
> cleanly onto powers (_fear_), but sometimes these powers are permanent
> (_silence_), or there simply exists no corresponding effect (regrowing body
> parts). Some creativity is required to create a compelling aberrant
> dragonmark, and the GM may have to adjust the rules to accommodate the player
> with their unique aberrant dragonmark.

### Arcane Background (Artificer)

This Edge is identical to **Arcane Background (Tinkerer)** from the _Savage
Worlds Fantasy Companion_.

### Arcane Background (Sorcerer)

Instead of Spellcasting, the sorcerer uses Focus as their arcane skill.

## New Powers

### Arcane Mark

**Rank:** Novice  
**Power Points:** 1  
**Range:** Touch  
**Duration:** Instant  
**Trappings:** Enchanted paper, arcane runes, magically transcribed spoken
words.

The caster creates a small unique _arcane mark_ on a surface. The _mark_ can be
visible or invisible. _Detect arcana_ will reveal an _arcane mark_.

#### Modifiers

- **Illusory Script (+2):** Instead of a mark, the caster writes a message. Any
  creatures designated when the power is cast can read the message as normal. To
  all others, the writing appears to be an unintelligible script or an entirely
  different message.

### Ward

**Rank:** Novice  
**Power Points:** 1  
**Range:** Smarts  
**Duration:** Eight hours  
**Trappings:** Arcane sigils, magical tripwire, enchanted dust.

The caster spends one minute to place a defensive enchantment on an object or
surface no bigger than a Small Blast Template. The _ward_ is not invisible, but
difficult to discern with the eye. Notice rolls made to detect the _ward_ suffer
a -2 penalty, or -4 with a raise.

The caster determines how _wards_ trigger (within Range), and by whom they can
be triggered. Once a _ward_ is triggered, the power ends.

As part of casting _ward_, the caster determines what will happen when the
_ward_ is triggered. Choose one of the effects below:

- Every target within the _ward_'s Blast Template suffers 2d6 damage, or 3d6
  with a raise. This blast may be **Evaded** (see _Savage Worlds_). The blast
  may be centred on the ward itself, the creature or object that triggered the
  _ward_, or somewhere else within Range determined in advance.
- Another power is set into the _ward_. As part of casting the power, the caster
  also casts another power, suffering a Multi-Action penalty, but only needs to
  roll once for both actions. When the _ward_ is triggered, the enclosed power
  is released. The target of the power may be the creature or object that
  triggered the _ward_, or something else within Range determined in advance.
- The _ward_ has a different effect---GM's call. This may cost more Power Points
  or cause the caster to suffer penalties to their roll.
- Nothing happens.

#### Modifiers

- **Alarm (+0/+1/+2):** For no extra cost, the _ward_ creates a loud sound at a
  volume up to the sound of a loud shout. For +1 points the caster is mentally
  alerted if they are within one mile, or within five miles for +2 points.
- **Area of Effect (+1/+2):** For +1 point _ward_ affects a Medium Blast
  Template, or a Large Blast Template for +2 points.
- **Permanent (0):** The _ward_ becomes permanent. The Power Points used to
  create it are "invested" and become unavailable until it is deactivated. The
  caster may terminate the _ward_ at any time, regardless of sight, distance, or
  other factors. Their Power Points begin recharging normally. Permanent _wards_
  remain active even if their creator is slain.

# Bestiary

## Aberrations

### Dolgaunt

**Attributes:** Agility d10, Smarts d8, Spirit d10, Strength d8, Vigor d10  
**Skills:** Athletics d8, Common Knowledge d4, Fighting d8, Intimidation d8,
Notice d8, Stealth d8, Taunt d8  
**Pace:** 6; **Parry:** 7; **Toughness:** 7  
**Hindrances:** Blind  
**Edges:** Combat Reflexes, Frenzy  
**Special Abilities:**

- **Blindsense:** Dolgaunts ignore invisibility, illusion, and all Illumination
  penalties.
- **Tentacles:** Str, Reach 1. If the dolgaunt hits, it may immediately make a
  grappling attempt (at +2) as a free action. A dolgaunt has two tentacles.
- **Very Resilient:** Dolgaunts can take two Wounds before they're
  Incapacitated.
- **Vitality Drain:** Dolgaunts can absorb life from a grappled creature through
  its tentacle as a Fighting roll. On a hit, the victim is affected by Energy
  Drain (Vigor). They roll Vigor (at -2 if Bound) to resist this effect. The
  dolgaunt recovers 1 Wound when the Energy Drain is successful.

### Dolgrim

**Attributes:** Agility d8, Smarts d4, Spirit d6, Strength d6, Vigor d6  
**Skills:** Athletics d6, Fighting d6, Intimidation d6, Notice d6, Shooting d8,
Stealth d10, Taunt d6  
**Pace:** 6; **Parry:** 6 (1); **Toughness:** 4  
**Hindrances:** Bloodthirsty, Ruthless (Major)  
**Edges:** Ambidextrous, Combat Reflexes  
**Gear:** Spear (Str+d6, Reach 1), mace (Str+d6), hand crossbow (Range 10/20/40,
2d6, AP 2), small shield (Parry +1)  
**Special Abilities:**

- **Darkvision:** Dolgrims ignore penalties for Illumination up to 10" (20
  yards).
- **Four Arms:** Dolgrims ignore 2 points of Multi-Action penalties each turn.
- **Size -1:** Dolgrims stand 3--4' tall.

## Beasts

### Dragonhawk

**Attributes:** Agility d8, Smarts d6 (A), Spirit d8, Strength d12+2, Vigor
d10  
**Skills:** Athletics d10, Fighting d8, Intimidation d10, Notice d12, Stealth
d6  
**Pace:** 4; **Parry:** 6; **Toughness:** 11  
**Edges:** Alertness, Extraction (Imp)  
**Special Abilities:**

- **Bite/Claws:** Str+d6.
- **Blindsense:** Dragonhawks ignore invisibility, illusion, and all
  Illumination penalties.
- **Flight:** Pace 24.
- **Low Light Vision:** Dragonhawks ignore penalties for Dim and Dark
  Illumination.
- **Size 4 (Large):** Dragonhawks are over 15' wide in wing span.

### Horse, Valenar Riding

**Attributes:** Agility d8, Smarts d4 (A), Spirit d8, Strength d12, Vigor d8  
**Skills:** Athletics d12, Fighting d6, Notice d6  
**Pace:** 16; **Parry:** 5; **Toughness:** 8  
**Edges:** ---  
**Special Abilities:**

- **Kick:** Str+d4.
- **Size 2:** Valenar horses typically weigh about 1,000 lb.
- **Speed:** d10 running die.

## Constructs

### Expeditious Messenger

**Attributes:** Agility d8, Smarts d4, Spirit d6, Strength d4-2, Vigor d6  
**Skills:** Athletics d8, Fighting d6, Notice d10, Persuasion d4, Stealth d8  
**Pace:** 3; **Parry:** 5; **Toughness:** 2  
**Edges:** ---  
**Special Abilities:**

- **Bite/Claws:** Str+d4.
- **Construct:** +2 to recover from being Shaken; ignores 1 point of Wound
  penalties; does not breathe or suffer from disease or poison.
- **Flight:** Pace 48.
- **Low Light Vision:** Expeditious messengers ignore penalties for Dim and Dark
  Illumination.
- **Size -3 (Very Small):** Expeditious messengers are tiny, speedy fliers,
  often looking like mechanical birds or sprites.
- **Telepathic Bond:** While on the same plane of existence as its master, the
  expeditious messenger can convey what it senses to its master, and the two can
  communicate telepathically.

### Living Spell

Living spells have an associated power. A _living burning hands_ might have
_burst_ as associated power, _living cure wounds_ might have _healing_, and
_living lightning bolt_ might have _bolt_.

**Attributes:** Agility d8, Smarts d4 (A), Spirit d4, Strength d12, Vigor d6  
**Skills:** Fighting d4, Notice d4, Spellcasting d12  
**Pace:** ---; **Parry:** 4; **Toughness:** 9  
**Edges:** Arcane Resistance  
**Special Abilities:**

- **Amorphous:** Can maneuver through any non-solid surface, pass through cracks
  in doors or windows, etc.
- **Hover:** Pace 4.
- **Immunity:** Immune to damage types associated with their associated power's
  trapping.
- **Living Spell:** +2 to recover from being Shaken; no additional damage from
  Called Shots; ignores 1 point of Wound penalties; doesn't breathe; cannot be
  blinded, deafened, or grappled; immune to Fear, Intimidation, Fatigue, and
  mind-affecting powers.
- **Magical Strike:** Offensive living spells can strike out and do Str+d6
  damage.
- **Size 4 (Large):** Living spells are 4 yards in diameter.
- **Spell Mimicry:** The living spell can cast their associated power as a
  limited action using its Spellcasting skill for the roll.

## Deathless

### :spades: Undying Councilor

**Attributes:** Agility d4, Smarts d10, Spirit d12, Strength d6, Vigor d8-1  
**Skills:** Academics d12, Athletics d4, Common Knowledge d10, Cosmology d10,
Faith d12, Fighting d4, Notice d10, Persuasion d10, Research d12, Science d12,
Shooting d4, Stealth d4  
**Pace:** 6; **Parry:** 4; **Toughness:** 7  
**Edges:** Arcane Background (Wizard), Aristocrat, Elan, Fey Blood, Iron Will  
**Powers:** _Arcane protection_, _banish_, _blast_, _bolt_, _boost trait_,
_detect/conceal arcana_, _dispel_, _divination_, _healing_, _light_, _scrying_;
**Power Points:** 50  
**Special Abilities:**

- **Deathless:** -4 to resist necrotic magic; +4 damage from necrotic magic; +2
  Toughness; +2 to recover from being Shaken; no additional damage from Called
  Shots; ignores 1 point of Wound penalties; doesn't breathe; immune to disease,
  poison, and radiant magic.
- **Frail:** Elves decrease their Toughness by 1 and suffer a -1 penalty to
  Vigor rolls.
- **Illumination:** The councilor sheds bright illumination in a Medium Blast
  Template centered on itself. The light can be extinguished or restored as a
  free action.
- **Darkvision:** Undying councilors ignore penalties for Illumination up to 10"
  (20 yards).

### Undying Soldier

**Attributes:** Agility d10, Smarts d6, Spirit d10, Strength d10, Vigor d8-1  
**Skills:** Academics d8, Athletics d10, Battle d6, Common Knowledge d6,
Cosmology d6, Fighting d10, Intimidation d8, Notice d8, Persuasion d8, Shooting
d10, Stealth d6  
**Pace:** 6; **Parry:** 9 (2); **Toughness:** 11 (4)  
**Edges:** Aristocrat, Counterattack (Imp), Fey Blood, Soldier  
**Gear:** Spear (Str+d6, Reach 1), large shield (Parry +2), bronzewood armor
(+4)  
**Special Abilities:**

- **Deathless:** -4 to resist necrotic magic; +4 damage from necrotic magic; +2
  Toughness; +2 to recover from being Shaken; no additional damage from Called
  Shots; ignores 1 point of Wound penalties; doesn't breathe; immune to disease,
  poison, and radiant magic.
- **Frail:** Elves decrease their Toughness by 1 and suffer a -1 penalty to
  Vigor rolls.
- **Illumination:** The soldier sheds bright illumination in a Medium Blast
  Template centered on itself. The light can be extinguished or restored as a
  free action.
- **Darkvision:** Undying soldiers ignore penalties for Illumination up to 10"
  (20 yards).
- **Resilient:** Undying soldiers can take one Wound before they're
  Incapacitated.

## Dinosaurs

### Fastieth

**Attributes:** Agility d8, Smarts d4 (A), Spirit d6, Strength d8, Vigor d6  
**Skills:** Athletics d8, Fighting d6, Intimidation d6, Notice d6, Stealth d6,
Survival d6  
**Pace:** 10; **Parry:** 5; **Toughness:** 5  
**Edges:** ---  
**Special Abilities:**

- **Bite:** Str+d4.
- **Speed:** d8 running die.

## Humanoids

### :spades: Bone Knight

**Attributes:** Agility d6, Smarts d6, Spirit d10, Strength d10, Vigor d8  
**Skills:** Academics d8, Athletics d8, Battle d8, Common Knowledge d6, Faith
d10, Fighting d10, Intimidation d10, Notice d6, Persuasion d8, Riding d8,
Shooting d8, Stealth d4, Taunt d6  
**Pace:** 6; **Parry:** 7; **Toughness:** 12 (6)  
**Hindrances:** Code of Honor (Major), Vow (Major)  
**Edges:** Arcane Background (Miracles), Command, Fervor, Sweep  
**Gear:** Greatsword (Str+d10), longbow (Range 15/30/60, 2d6, AP 1), bonecraft
armor (+6)  
**Powers:** _Darkness_, _puppet_, _smite_, _zombie_; **Power Points:** 15

### :spades: Inspired

**Attributes:** Agility d6, Smarts d12, Spirit d12+1, Strength d6, Vigor d6  
**Skills:** Academics d10, Athletics d4, Common Knowledge d8, Cosmology d10,
Fighting d6, Intimidation d8, Notice d10, Persuasion d12, Psionics d12, Stealth
d6, Taunt d6  
**Pace:** 6; **Parry:** 5; **Toughness:** 5  
**Edges:** Arcane Background (Psionics), Aristocrat, Attractive, Combat
Reflexes, Command, Charismatic, Mentalist  
**gear:** Crysteel dagger (Str+d4+2)  
**Powers:** _Confusion_, _empathy_, _fear_, _mind link_, _mind reading_, _mind
wipe_, _puppet_, _scrying_, _slumber_; **Power Points:** 50  
**Special Abilities:**

- **Dual Mind:** Inspired get a free reroll when rolling to resist hostile
  psionic powers and effects.
- **Fearless:** Inspired are immune to Fear and Intimidation.

### Knight Arcane/Knight Phantom

**Attributes:** Agility d8, Smarts d6, Spirit d6, Strength d8, Vigor d6  
**Skills:** Athletics d6, Common Knowledge d6, Fighting d8, Intimidation d6,
Notice d6, Persuasion d6, Riding d8, Shooting d8, Spellcasting d8, Stealth d4  
**Pace:** 6; **Parry:** 8 (2); **Toughness:** 9 (4)  
**Edges:** Arcane Background (Magic), Soldier  
**Powers:** _Blast_, _bolt_, _burst_, _detect/conceal arcana_, _protection_,
_smite_, _summon ally_ (phantom steed, 4 Power Points, Duration one hour);
**Power Points:** 15  
**Gear:** Wand of Bolts, arming sword (Str+d8), plate armor (+4), medium shield
(Parry +2)

#### Phantom Steed

**Attributes:** Agility d8, Smarts d4 (A), Spirit d6, Strength d12, Vigor d8  
**Skills:** Athletics d8, Fighting d4, Notice d6  
**Pace:** 16; **Parry:** 4; **Toughness:** 8  
**Edges:** Fleet-Footed  
**Special Abilities:**

- **Ethereal:** Phantom steeds are quasi-real. They can become immaterial (but
  not invisible) at will to all except the caster. They can only be harmed by
  magical attacks.
- **Kick:** Str+d4, to the front or rear as desired.
- **Size 2:** Phantom steeds are as tall as riding horses, even if they do not
  actually weigh anything.

### Wandslinger

**Attributes:** Agility d6, Smarts d6, Spirit d6, Strength d6, Vigor d6  
**Skills:** Athletics d6, Common Knowledge d6, Fighting d4, Intimidation d6,
Notice d6, Persuasion d6, Shooting d6, Stealth d4  
**Pace:** 6; **Parry:** 4; **Toughness:** 6 (+1)  
**Edges:** ---  
**Gear:** Wand of Bolts, shortsword (Str+d6), leather armor (+1)

### Warforged Soldier

**Attributes:** Agility d6, Smarts d6, Spirit d6, Strength d8, Vigor d8  
**Skills:** Athletics d8, Battle d6, Fighting d8, Intimidation d6, Notice d6,
Shooting d8, Stealth d4, Survival d4  
**Pace:** 6; **Parry:** 8 (2); **Toughness:** 10 (4)  
**Hindrances:** Quirk  
**Edges:** Soldier  
**Gear:** Longsword (Str+d8), longbow (Range 15/30/60, 2d6, AP 1), medium shield
(Parry +2)  
**Special Abilities:**

- **Armor +4:** A warforged is covered in tough plating made of
  magically-treated woods and metals.
- **Living Construct:** +2 to recover from being Shaken; ignore 1 point of Wound
  modifiers; does not breathe or suffer from disease or poison.
- **Resilient:** Warforged soldiers can take one Wound before they're
  Incapacitated.

## Valenar Animals

To create your own Valenar animal, take a beast template and give it Smarts,
Academics, Common Knowledge, Persuasion, and Bonding.

### Valenar Hawk

**Attributes:** Agility d8, Smarts d6, Spirit d6, Strength d4-2, Vigor d6  
**Skills:** Academics d6, Athletics d8, Common Knowledge d6, Fighting d6, Notice
d10, Persuasion d6, Stealth d8  
**Pace:** 3; **Parry:** 5; **Toughness:** 2  
**Edges:** ---  
**Special Abilities:**

- **Bite/Claws:** Str+d4.
- **Bonding:** Valenar animals can magically bond with creatures. The bond lasts
  until the animal bonds with a different creature, or the bonded creature dies.
  While bonded, the animal and the bonded creature can communicate
  telepathically as per the _mind link_ power.
- **Flight:** Pace 48.
- **Size -3 (Very Small):** Valenar hawks are light-weight and about two feet
  tall.

### Valenar Hound

**Attributes:** Agility d8, Smarts d6, Spirit d6, Strength d6, Vigor d6  
**Skills:** Academics d6, Athletics d8, Common Knowledge d6, Fighting d6, Notice
d10, Persuasion d6, Stealth d8  
**Pace:** 8; **Parry:** 5; **Toughness:** 4  
**Edges:** Alertness  
**Special Abilities:**

- **Bite:** Str+d4.
- **Bonding:** Valenar animals can magically bond with creatures. The bond lasts
  until the animal bonds with a different creature, or the bonded creature dies.
  While bonded, the animal and the bonded creature can communicate
  telepathically as per the _mind link_ power.
- **Size -1:** The heads of Valenar hounds come to an average human's waist, and
  they weigh about 60 pounds.
- **Speed:** d10 running die.

### Valenar Steed

**Attributes:** Agility d8, Smarts d6, Spirit d8, Strength d12, Vigor d8  
**Skills:** Academics d6, Athletics d12, Common Knowledge d6, Fighting d6,
Notice d6, Persuasion d6  
**Pace:** 16; **Parry:** 5; **Toughness:** 8  
**Edges:** ---  
**Special Abilities:**

- **Bonding:** Valenar animals can magically bond with creatures. The bond lasts
  until the animal bonds with a different creature, or the bonded creature dies.
  While bonded, the animal and the bonded creature can communicate
  telepathically as per the _mind link_ power.
- **Kick:** Str+d4.
- **Size 2:** Valenar horses typically weigh about 1,000 lb.
- **Speed:** d10 running die.

## Undead

### Karrnathi Skeleton

**Attributes:** Agility d6, Smarts d4, Spirit d6, Strength d8, Vigor d6  
**Skills:** Athletics d8, Battle d4, Fighting d8, Intimidation d8, Notice d4,
Shooting d8, Stealth d6  
**Pace:** 6; **Parry:** 6; **Toughness:** 10 (3)  
**Edges:** Soldier  
**Gear:** Longsword (Str+d8), longbow (Range 15/30/60, 2d6, AP 1), half plate
armor (+3)  
**Special Abilities:**

- **Fearless:** Skeletons are immune to Fear and Intimidation.
- **Darkvision:** Skeletons ignore penalties for Illumination up to 10" (20
  yards).
- **Resilient:** Karrnathi skeletons can take one Wound before they're
  Incapacitated.
- **Undead:** +2 Toughness; +2 to recover from being Shaken; no additional
  damage from Called Shots; ignores 1 point of Wound penalties; doesn't breathe;
  immune to disease and poison.

### Karrnathi Zombie

**Attributes:** Agility d6, Smarts d4, Spirit d6, Strength d8, Vigor d10  
**Skills:** Athletics d8, Battle d4, Fighting d8, Intimidation d8, Notice d4,
Shooting d8, Stealth d6  
**Pace:** 4; **Parry:** 6; **Toughness:** 12 (3)  
**Edges:** Soldier  
**Gear:** Longsword (Str+d8), longbow (Range 15/30/60, 2d6, AP 1), half plate
armor (+3)  
**Special Abilities:**

- **Fearless:** Zombies are immune to Fear and Intimidation.
- **Darkvision:** Zombies ignore penalties for Illumination up to 10" (20
  yards).
- **Resilient:** Karrnathi zombies can take one Wound before they're
  Incapacitated.
- **Undead:** +2 Toughness; +2 to recover from being Shaken; no additional
  damage from Called Shots; ignores 1 point of Wound penalties; doesn't breathe;
  immune to disease and poison.
- **Weakness (Head):** Called Shots to a zombie's head do the usual +4 damage.
