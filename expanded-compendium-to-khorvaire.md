<!--
SPDX-FileCopyrightText: © 2020 Carmen Bianca Bakker <carmen@carmenbianca.eu>
SPDX-FileCopyrightText: 2020 Kristian Serrano
SPDX-FileCopyrightText: 2000-2003, 2016 Wizards of the Coast, Inc.

SPDX-License-Identifier: CC-BY-4.0
-->

\onecolumn

# Credits {-}

**Written & designed by** Carmen Bianca Bakker

**Inspired by**
[_Eberron for Savage Worlds_](https://blog.immaterialplane.com/2019/02/eberron-for-savage-worlds.html)
by Kristian Serrano

**Eberron created by** Keith Baker and others

**Savage Worlds written & designed by** Shane Lacy Hensley, with Clint Black

_Wild Cards' Expanded Compendium to Khorvaire_ is unofficial Fan Content
permitted under the
[Fan Content Policy](https://company.wizards.com/fancontentpolicy). Not
approved/endorsed by Wizards. Portions of the materials used are property of
Wizards of the Coast. ©Wizards of the Coast LLC.

Dungeons & Dragons, D&D, Eberron and its respective logos are trademarks of
Wizards of the Coast in the USA and other countries.

This game references the _Savage Worlds_ game system, available from Pinnacle
Entertainment Group at <https://www.peginc.com>. Savage Worlds and all
associated logos and trademarks are copyrights of Pinnacle Entertainment Group.

Portions of this work are copyright Kristian Serrano, and used with permission
under the condition that the author be attributed. See the hyperlink to _Eberron
for Savage Worlds_ above.

All parts of this work over which the author holds copyright are licensed under
a
[Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).
© 2020 Carmen Bianca Bakker

\twocolumn

# Foreword

_Wild Cards' Expanded Compendium to Khorvaire_ is a document that is intended to
contain fluff that would be superfluous in the main document. Why two documents?
Because I intend the main document to be as light as possible. Just enough rules
for your Eberron game. This document contains everything else.

For the main _Wild Cards' Compendium to Khorvaire_, see this link:
<https://carmenbianca.gitlab.io/compendium-to-khorvaire/>

Versions of this document are available at:

- HTML --- <https://carmenbianca.gitlab.io/compendium-to-khorvaire/expanded/>
- EPUB ---
  [https://carmenbianca.gitlab.io/compendium-to-khorvaire/Wild Cards' Expanded Compendium to Khorvaire.epub](https://carmenbianca.gitlab.io/compendium-to-khorvaire/Wild%20Cards%27%20Expanded%20Compendium%20to%20Khorvaire.epub)
- PDF ---
  [https://carmenbianca.gitlab.io/compendium-to-khorvaire/Wild Cards' Expanded Compendium to Khorvaire.pdf](https://carmenbianca.gitlab.io/compendium-to-khorvaire/Wild%20Cards%27%20Expanded%20Compendium%20to%20Khorvaire.pdf)

# Races

## Aarakocra

- **Talons (+1):** An aarakocra's claws are **Natural Weapons** (see _Savage
  Worlds_) that cause Strength+d4 damage.
- **Flight (+4):** Aarakocra fly at Pace 12. Their running die for flying is a
  d6.
- **Reduced Pace (-1):** Decrease the character's Pace by 1 and their running
  die one die type.

## Centaurs

<!--
Almost identical to Fantasy Companion, but added Can't Swim, Charge, and
Exposed Flanks
-->

- **Can't Swim (-1):** Centaurs have the Can't Swim Hindrance.
- **Charge (+2\*):** Centaurs have the Charge Edge.
- **Equine Build (-2):** Centaurs cannot ride mounts or use certain types of
  armor, gear, or other equipment that does not fit their form (GM's call).
- **Exposed Flanks (-1):** Centaurs are easier to strike in the flanks. They
  have -1 Parry.
- **Hooves (+1):** A centaur's hooves are **Natural Weapons** (see _Savage
  Worlds_) that cause Strength+d4 damage.
- **Pace (+4):** The Pace of centaurs is increased by 4, and their running die
  by two die types.
- **Size +1 (+1):** Centaurs are human-sized creatures with the lower body of a
  horse, increasing their Size (and therefore Toughness) by 1.

## Dragonborn

<!-- Almost-verbatim copy from 5e. -->

- **Breath Weapon (+2):** Dragonborn can breathe destructive energy by making an
  Athletics roll as a limited action. This uses the Cone Template, may be
  Evaded, and causes 2d6 damage (3d6 with a raise on the Athletics roll). A
  Critical Failure on the attack causes Fatigue. Their ancestry's energy type
  determines an additional effect:
  - **Acid:** Anyone Shaken or Wounded by the breath weapon automatically takes
    2d4 damage at the start of their next turn. If the target is wearing armor,
    roll a d6. On a 5 or 6 the armor loses one point of protection.
  - **Cold:** If Shaken or Wounded by the attack, the target's Pace is reduced
    by 2 for five rounds. These effects are cumulative to a minimum Pace of 1.
  - **Fire:** The breath weapon causes +2 damage and may set the target on fire.
  - **Lightning:** Anyone Shaken or Wounded by the attack must make a Vigor roll
    or be Stunned.
- **Environmental Resistance (+1):** Dragonborn receive a +4 bonus to resist a
  negative environmental effect determined by their draconic ancestry, such as
  heat, cold, acid, poison, or lightning. Damage from that source is also
  reduced by 4.
- **Size +1 (+1):** Dragonborn are tall creatures, increasing their Size (and
  therefore Toughness) by 1.

## Fairies (Sprites/Pixies)

<!-- TODO -->

- **Diminutive (+6):** Fairies are Tiny, reducing their Size (and therefore
  Toughness) by 4. They subtract 4 from damage rolls, and have a maximum
  Strength of d4. Their Scale Modifier is -6.
- **Flight (+2):** Fairies fly at Pace 6.
- **Not Imposing (-2):** Fairies have a -2 penalty to Intimidation rolls.

## Firbolgs

- **Firbolg Magic (+3):** Firbolgs can cast _disguise_ and _detect arcana_ on
  themselves as limited free actions. They may appear one Size smaller when
  disguised. They can also cast _invisibility_ on themselves as a limited free
  action once every 10 rounds, and its Duration is 1 round when cast in this
  way.
- **Size +1 (+1):** The average firbolg stands between 7' and 8' tall,
  increasing their Size (and therefore Toughness) by 1.
- **Speech of Beast and Leaf (+0):** Firbolgs have a limited capacity to
  communicate ideas to beasts and plants, but cannot understand them in return.

## Gargoyles

- **Bite/Claws (+1):** A gargoyle's fangs and claws are **Natural Weapons** (see
  _Savage Worlds_) that cause Strength+d4 damage.
- **Big (-2):** Gargoyles subtract two when using equipment designed for smaller
  beings, and cannot wear humanoid armor or clothing. Equipment, armor, food and
  clothing cost double the listed price.
- **Darkvision (+1):** Gargoyles ignore penalties for Illumination up to 10" (20
  yards).
- **Doesn't Breathe (+1):** Gargoyles do not breathe.
- **False Appearance (+1):** Gargoyles blend with natural stone, granting them
  +2 to Stealth rolls when perfectly still.
- **Flight (+2):** Gargoyles fly at Pace 8.
- **Stone Skin (+1):** Gargoyles have a physique made of stone and gain Armor
  +2.
- **Weakness to Magic (-1):** Magical damage ignores 4 points of Armor provided
  by Stone Skin.

## Genasi, Air

- **Air Manipulation (+1):** Air genasi can cast _elemental manipulation_ (air
  only) using Athletics without expending Power Points.
- **Air Resistance (+1):** Air genasi receive a +4 bonus to resist aerial
  effects (wind, thunder, lightning, etc.). Damage from these sources is reduced
  by 4.
- **Lesser Dependency (-1):** Genasi must spend a few moments every day in close
  proximity to their element. They take a level of Fatigue each day if they
  can't, and recover a level per minute spent close to their element. This
  cannot lead to Incapacitation.
- **Levitate (+1):** Air genasi can levitate close to the ground and never take
  damage from falling.
- **Unending Breath (+2):** Air genasi can hold their breath indefinitely.

## Genasi, Earth

- **Darkvision (+1):** Earth genasi ignore penalties for Illumination up to 10"
  (20 yards).
- **Earth Manipulation (+1):** Earth genasi can cast _elemental manipulation_
  (earth only) using Athletics without expending Power Points.
- **Earth Resistance (+1):** Earth genasi receive a +4 bonus to resist
  earth-based effects. Damage from these sources is reduced by 4.
- **Earth Walk (+1):** Earth genasi ignore penalties for Difficult Ground made
  of earth or stone. They can pass over the ground without leaving a trace.
- **Lesser Dependency (-1):** Genasi must spend a few moments every day in close
  proximity to their element. They take a level of Fatigue each day if they
  can't, and recover a level per minute spent close to their element. This
  cannot lead to Incapacitation.
- **Toughness +1 (+1\*):** The element of earth grants earth genasi extra
  resilience. They have Toughness +1.

## Genasi, Fire

- **Fire Focus (+2\*):** Fire genasi can create a burst of fire by making an
  Athletics roll as a limited action. This uses the Cone Template, may be
  Evaded, and causes 2d6 damage (3d6 with a raise on the Athletics roll). This
  has a chance to set someone on fire (see **Hazards** in the _Savage Worlds_).
  A Critical Failure on the attack causes Fatigue.
- **Fire Manipulation (+1):** Fire genasi can cast _elemental manipulation_
  (fire only) using Athletics without expending Power Points.
- **Fire Resistance (+1):** Fire genasi receive a +4 bonus to resist fire-based
  effects (including heat). Damage from these sources is reduced by 4.
- **Infravision (+1):** Fire genasi can see in the infrared spectrum, halving
  penalties for bad lighting when attacking targets that radiate heat.
- **Lesser Dependency (-1):** Genasi must spend a few moments every day in close
  proximity to their element. They take a level of Fatigue each day if they
  can't, and recover a level per minute spent close to their element. This
  cannot lead to Incapacitation.

## Genasi, Water

- **Amphibious (+2):** Water genasi can breathe underwater and move their full
  Pace when swimming.
- **Low Light Vision (+1):** Water genasi ignore penalties for Dim and Dark
  Illumination.
- **Lesser Dependency (-1):** Genasi must spend a few moments every day in close
  proximity to their element. They take a level of Fatigue each day if they
  can't, and recover a level per minute spent close to their element. This
  cannot lead to Incapacitation.
- **Water Manipulation (+1):** Water genasi can cast _elemental manipulation_
  (water only) using Athletics without expending Power Points.
- **Water Resistance (+1):** Water genasi receive a +4 bonus to resist aquatic
  effects (including frost and ice). Damage from these sources is reduced by 4.

## Gnolls

- **Bite (+1):** A gnolls's fangs are **Natural Weapons** (see _Savage Worlds_)
  that cause Strength+d4 damage.
- **Darkvision (+1):** Gnolls ignore penalties for Illumination up to 10" (20
  yards).
- **Pack Tactics (+1\*):** Gnolls add any Gang Up bonus to their Fighting damage
  rolls.
- **Size +1 (+1):** The average gnoll stands taller than 7', increasing their
  Size (and therefore Toughness) by 1.

## Goliaths

- **Armor +2 (+1):** Goliath skin is especially tough, granting them Armor +2.
- **Powerful Build (+2\*):** Goliaths start with a d6 in Strength instead of a
  d4. This increases maximum Strength to d12 + 1.
- **Size +1 (+1):** Goliaths are 7--8 feet of muscle, increasing their Size (and
  therefore Toughness) by 1.

## Grungs

- **Amphibious (+1):** Grungs can breathe underwater.
- **Natural Climber (+1\*):** Grungs can climb vertical surfaces as though
  walking normally, or inverted surfaces at half Pace.
- **Poison Immunity (+1):** Grungs are immune to poison.
- **Poisonous Skin (+3):** With a successful regular or Touch Attack, the victim
  must roll Vigor or suffer the effects of Mild Poison. They also suffer an
  additional effect for 2d4 rounds depending on the grung's skin color.
  - **Blue:** The victim must shout loudly or otherwise make a loud noise as a
    free action on their turn.
  - **Gold:** The victim regards the grung as a friend and can speak Grung. The
    grung gains +1 to Intimidation, Persuasion, Performance, or Taunt rolls
    against the victim.
  - **Green:** The victim can't move except to climb or jump.
  - **Orange:** The victim is frightened of their allies. While in the presence
    of their allies, they are Distracted.
  - **Purple:** The victim feels a desperate need to soak themself in liquid or
    mud. They are Distracted unless immersed in a body of liquid or mud.
  - **Red:** The victim is incredibly hungry. They must use at least one action
    each turn to eat. If they do not eat, they are Distracted.
- **Reduced Pace (-1):** Decrease the character's Pace by 1 and their running
  die one die type.
- **Size -1 (-1):** Grungs average about three feet tall, reducing their Size
  (and therefore Toughness) by 1.
- **Leaper (+2\*):** Grungs can jump twice as far as listed under **Movement**
  (see _Savage Worlds_). In addition, grungs add +4 to damage when leaping as
  part of a Wild Attack instead of the usual +2 (unless in a closed or confined
  space where they cannot leap horizontally or vertically---GM's call).
- **Water Dependency (-2):** Grungs must immerse themselves in water one hour
  out of every 24 or become automatically Fatigued each day until they are
  Incapacitated. The day after Incapacitation from dehydration, they perish.
  Each hour spent in water restores one level of Fatigue.

## Harpies

- **Claws (+1):** A harpy's claws are **Natural Weapons** (see _Savage Worlds_)
  that cause Strength+d4 damage.
- **Flight (+2):** Harpies fly at Pace 8. Their running die for flying is a d6.
- **Reduced Pace (-1):** Decrease the character's Pace by 1 and their running
  die one die type.
- **Supernatural Song (+2):** When harpies use their supernatural voice and song
  to influence others using Performance, they receive a +2 bonus to their roll.

## Kenku

- **Agile (+2\*):** Kenku start with a d6 in Agility instead of a d4. This
  increases maximum Agility to d12 + 1
- **Low Light Vision (+1):** Kenku ignore penalties for Dim and Dark
  Illumination.
- **Mimicry (+1):** Kenku are able to perfectly reproduce any sound or voice
  they have heard, and can duplicate handwriting and handicrafts they have seen.
  A creature rolls Smarts -2 if they want to discern that the sound or object is
  an imitation.
- **Nimble (+1\*):** Kenku start with a d4 in Thievery.
- **Size -1 (-1):** Kenku are a foot shorter than the average human, and are
  lightweight avians, reducing their Size (and therefore Toughness) by 1.

## Kobolds

<!-- TODO -->

- **Armor +2 (+1):** Kobolds have tough, scaly skin, increasing their Armor
  by 2.
- **Agile (+2\*):** Kobolds start with a d6 in Agility instead of a d4. This
  increases maximum Agility to d12 + 1
- **Darkvision (+1):** Kobolds ignore penalties for Illumination up to 10" (20
  yards).
- **Pack Tactics (+1\*):** Kobolds add any Gang Up bonus to their Fighting
  damage rolls.
- **Size -1 (-1):** Kobolds are between two and three feet, reducing their Size
  (and therefore Toughness) by 1.
- **Sunlight Sensitivity (-2):** Kobolds are Distracted in sunlight.

> #### Variant: Winged Kobolds
>
> Replace Agile or Pack Tactics with the following feature:
>
> - **Flight (+2):** Winged kobolds fly at Pace 6.

## Kuo-toa

- **Amphibious (+2):** Kuo-toa can breathe underwater and move their full Pace
  when swimming.
- **Darkvision (+1):** Kuo-toa ignore penalties for Illumination up to 10" (20
  yards).
- **Otherworldly Perception (+1):** Kuo-toa can sense the presence of any moving
  creature within 10" (20 yards) that is invisible or ethereal. They gain a +2
  bonus to rolls to attack these creatures.
- **Slippery (+1):** Kuo-toa have a +2 bonus to rolls made to resist being
  grappled and rolls made to break free from being Bound or Entangled.
- **Sunlight Sensitivity (-2):** Kuo-toa are Distracted in sunlight.
- **Toughness +1 (+1):** Kuo-toa have a scaly skin, increasing their Toughness
  by 1.

## Locathah

<!-- TODO -->

- **Amphibious (+2):** Locathah can breathe underwater and move their full Pace
  when swimming.
- **Leviathan Will (+2\*):** Locathah receive a +2 bonus to resist poison, being
  Stunned, being put to sleep, and magical effects that alter their mood or
  feelings.
- **Toughness +1 (+1):** Locathah have a scaly skin, increasing their Toughness
  by 1.
- **Water Dependency (-2):** Locathah must immerse themselves in water one hour
  out of every 24 or become automatically Fatigued each day until they are
  Incapacitated. The day after Incapacitation from dehydration, they perish.
  Each hour spent in water restores one level of Fatigue.

## Merfolk

- **Creature of the Sea (+2):** Merfolk can breathe underwater. Their Pace is 8,
  and they move at their full Pace when swimming. Each inch moved on land costs
  3" of Pace, and they suffer a -2 penalty to Athletics rolls related to
  movement on land.
- **Low Light Vision (+1):** Merfolk ignore penalties for Dim and Dark
  Illumination.
- **Flexibility (+1):** A merfolk may start with a d6 in one Attribute instead
  of a d4. This does not increase the Attribute's maximum.

## Merrow

<!-- TODO: Technically Bite/Claws should be +1 each -->

- **Big (-2):** Merrow subtract two when using equipment designed for smaller
  beings, and cannot wear humanoid armor or clothing. Equipment, armor, food and
  clothing cost double the listed price.
- **Bite/Claws (+1):** A merrow's fangs and claws are **Natural Weapons** (see
  _Savage Worlds_) that cause Strength+d4 damage.
- **Creature of the Sea (+2):** Merrow can breathe underwater. Their Pace is 8,
  and they move at their full Pace when swimming. Each inch moved on land costs
  3" of Pace, and they suffer a -2 penalty to Athletics rolls related to
  movement on land.
- **Low Light Vision (+1):** Merrow ignore penalties for Dim and Dark
  Illumination.
- **Size +2 (+2):** Merrow are large sea creatures, increasing their Size (and
  therefore Toughness) by 2.

## Minotaurs

- **Big (-2):** Merrow subtract two when using equipment designed for smaller
  beings, and cannot wear humanoid armor or clothing. Equipment, armor, food and
  clothing cost double the listed price.
- **Horns (+2):** Minotaurs have horns that are **Natural Weapons** (see _Savage
  Worlds_) that cause Strength+d6 damage.
- **Powerful Build (+2\*):** Minotaurs start with a d6 in Strength instead of a
  d4. This increases maximum Strength to d12 + 1.
- **Size +2 (+2):** Minotaurs are hulking creatures, increasing their Size (and
  therefore Toughness) by 2.

## Sahuagin

- **Amphibious (+2):** Sahuagin can breathe underwater and move their full Pace
  when swimming.
- **Bite/Claws (+1):** A sahuagin's fangs and claws are **Natural Weapons** (see
  _Savage Worlds_) that cause Strength+d4 damage.
- **Blood Frenzy (+1\*):** When a sahuagin makes a Wild Attack against a
  bleeding creature, they add +4 to their damage instead of +2.
- **Darkvision (+1):** Sahuagin ignore penalties for Illumination up to 10" (20
  yards).
- **Toughness +1 (+1):** Sahuagin have a scaly skin, increasing their Toughness
  by 1.
- **Water Dependency (-2):** Sahuagin must immerse themselves in water one hour
  out of every 24 or become automatically Fatigued each day until they are
  Incapacitated. The day after Incapacitation from dehydration, they perish.
  Each hour spent in water restores one level of Fatigue.

## Sea Elves

- **Agile (+2\*):** Sea elves start with a d6 in Agility instead of a d4. This
  increases maximum Agility to d12 + 1.
- **Amphibious (+2):** Sea elves can breathe underwater and move their full Pace
  when swimming.
- **Fey Ancestry (+2):** Sea elves start with the Fey Blood Edge.
- **Frail (-3\*):** Sea elves decrease their Toughness by 1 and suffer a -1
  penalty to Vigor rolls.
- **Keen Senses (+1\*):** Sea elves start with a d6 in Notice instead of a d4.
  This increases maximum Notice to d12 + 1.
- **Low Light Vision (+1):** Sea elves ignore penalties for Dim and Dark
  Illumination.
- **Trance (+1):** Sea elves don't need to sleep. Instead, they meditate deeply,
  remaining semi-conscious. This trance lasts approximately three hours.
- **Water Dependency (-2):** Sea elves must immerse themselves in water one hour
  out of every 24 or become automatically Fatigued each day until they are
  Incapacitated. The day after Incapacitation from dehydration, they perish.
  Each hour spent in water restores one level of Fatigue.

## Tieflings

- **Darkvision (+1):** Tieflings ignore penalties for Illumination up to 10" (20
  yards).
- **Devil's Tongue (+2\*):** Tieflings start with a d6 in Spirit instead of a
  d4. This increases maximum Spirit to d12 + 1.
- **Hellish Resistance (+1):** Tieflings receive a +4 bonus to resist fire-based
  effects (including heat). Damage from these sources is reduced by 4.

> #### Variant: Winged Tieflings
>
> Replace Devil's Tongue with the following feature:
>
> - **Flight (+2):** Winged tieflings fly at Pace 6.

## Tortles

- **Armor +6 (+2):** Tortles have a strong shell, granting them Armor +6. This
  Armor applies exclusively to the tortle's torso. Tortles cannot wear other
  Armor on their torso.
- **Claws (+1):** A tortle's claws are **Natural Weapons** (see _Savage Worlds_)
  that cause Strength+d4 damage.
- **Semi-Aquatic (+1):** Tortles can hold their breath for 15 minutes.

# Edges

## New Edges

### Charming Song

**Requirements:** Novice, Harpy

The harpy gains the Empathy power. Once per hour, they can cast it, and its
modifiers, using Performance without expending Power Points.

### Improved Stone Skin

**Requirements:** Gargoyle

The gargoyle's stone skin is significantly stronger than normal. They gain
another Armor +2.

### Luring Song

**Requirements:** Veteran, Harpy

The harpy learns a magical melody that causes those who listen to it to want to
approach the source of the song.

The harpy makes a Performance roll. All non-harpy listeners of human
intelligence who can hear the song (generally within 50") must oppose the
harpy's Performance with a Spirit roll.

Those who fail are Distracted, can't take actions, and must move on their turn
toward the harpy by the most direct route. If the most direct route would cause
harm, such as stepping into lava or falling off a cliff, the target gets an
automatic Spirit roll as a free action to stop moving for the remainder of the
turn. Additionally, at the end of every turn, the target gets an automatic
Spirit roll as a free action to shake off the effect of the harpy's song.

The harpy must spend their entire turn singing without taking other actions to
maintain the song. The harpy cannot sing the song again until they wake up from
six hours' sleep.

# Powers

## Modified Powers

### Empathy

The Duration of _empathy_ is increased to 10 minutes.

_Empathy_ receives the following modifiers:

- **Ensorcell (+10):** Epic Power Modifier. The target falls madly in love
  (platonic or otherwise) with the caster, and will do anything for them. The
  power ends early if the caster or one of their companions do anything harmful
  to the target. When the power ends, the target knows they was ensorcelled by
  the caster.

### Puppet

_Puppet_ receives the following modifiers:

- **Suggestion (+0):** Instead of controlling the target, the caster makes a
  suggestion for a course of action that must sound reasonable. The target feels
  compelled to pursue the suggestion. The Duration of the power becomes one
  hour.

### Sloth/Speed

_Sloth/speed_ receives the following modifiers:

- **Feather Fall (+0):** _Sloth_ only. For no extra cost, the power reduces the
  target's rate of descent to 10" per round and they take no damage from
  falling. The target may choose not to shake off the effects of _sloth_.
- **Jump (+1):** _Speed_ only. The character's jump distance is tripled. This
  normally results in a horizontal jump distance of 3" and a vertical distance
  of 1.5".

## New Powers

### Awaken

**Rank:** Heroic  
**Power Points:** 30  
**Range:** Touch  
**Duration:** Instant  
**Trappings:** Elaborate ceremonies, rare plants, magical amulets, divine
intervention.

Whereas resurrection wills life back into that which once lived, _awaken_ wills
sapience into that which never thought.

The caster prays, meditates, chants, or otherwise concentrates on willing
sapience into an object, plant, or animal for four hours.

Once done, the caster makes a casting roll at -8. If successful, the target
becomes sapient and gains the ability to speak.

### Goodberry

**Rank:** Novice  
**Power Points:** 1  
**Range:** Touch  
**Duration:** Instant

The caster conjures a _goodberry_. Eating it removes one Fatigue level, or two
with a raise. It also removes a character's Shaken status, and removes the
Stunned status with a raise.

The _berry_ provides enough nourishment to sustain a creature for a day.

The _goodberry_ loses its potency if it has not been consumed within 24 hours of
being conjured.

#### Modifiers

- **Additional Berries (+1):** The power creates another _goodberry_ for 1
  additional Power Point each.
- **Healing Berry (+3):** The _goodberry_, once eaten, removes a Wound less than
  one hour old, or two Wounds with a raise. This modifier applies to only one
  _goodberry_ conjured by the power.

# Bestiary

## Aberrations

### Chuul

**Attributes:** Agility d6, Smarts d4, Spirit d8, Strength d12+2, Vigor d10  
**Skills:** Athletics d8, Fighting d8, Intimidation d8, Notice 8, Stealth d6,
Survival d8  
**Pace:** 6; **Parry:** 6; **Toughness:** 13 (4)  
**Edges:** Frenzy  
**Special Abilities:**

- **Aquatic:** Pace 6.
- **Armor +4:** Strong protective exoskeleton.
- **Darkvision:** Chuuls ignore penalties for Illumination up to 10" (20 yards).
- **Hardy:** The creature does not suffer a Wound from being Shaken twice.
- **Immunity:** Poison.
- **Pincers:** Str+d8, Reach 1. If the chuul hits with a raise, it may
  immediately make a grappling attempt as a free action.
- **Tentacles:** Chuuls can poison a creature with their mouth tentacles. After
  being hit by a successful **Touch Attack** (see _Savage Worlds_), the victim
  must make a Vigor roll to resist Paralyzing Poison.
- **Sense Magic:** Chuuls permanently enjoy the _detect arcana_ power. They
  cannot see concealed magic.
- **Size 2:** Chuuls are 8' tall yellow-green lobsters.
- **Resilient:** Chuuls can take one Wound before they're Incapacitated.

### Intellect Devourer

**Attributes:** Agility d6, Smarts d8, Spirit d6, Strength d4-2, Vigor d4  
**Skills:** Common Knowledge d8, Cosmology d8, Fighting d6, Notice d6, Shooting
d8, Stealth d10  
**Pace:** 8; **Parry:** 5; **Toughness:** 1  
**Edges:** Blind  
**Special Abilities:**

- **Blindsense:** Intellect devourers ignore invisibility, illusion, and all
  Illumination penalties.
- **Body Thief:** The intellect devourer makes an opposed Smarts roll against a
  Stunned or Incapacitated creature within 1". With a success, the intellect
  devourer magically consumes the target's brain, teleports into the target's
  skull, and takes total control of the target's body. It retains its own
  Smarts, Spirit, and linked skills, and gains the victim's Agility, Strength,
  Vigor, and all their skills, and learns everything the creature knew. If the
  host body dies, the intellect devourer must leave it.
- **Claws:** Str+d4.
- **Detect Sentience:** The intellect devourer can sense the presence and
  location of any sapient creature within 50" (100 yards) of it.
- **Devour Intellect:** The intellect devourer targets one creature with a brain
  within 2" with a Shooting roll. A hit means the target is affected by Energy
  Drain (Smarts), and they roll Smarts to resist this effect. The loss of Smarts
  is a permanent Injury.
- **Size -3 (Very Small):** An intellect devourer is the size of a brain.
- **Telepathy:** Intellect devourers can freely telepathically speak to
  creatures within 10".

### Spectator

**Attributes:** Agility d6, Smarts d10, Spirit d6, Strength d4, Vigor d6  
**Skills:** Common Knowledge d4, Cosmology d8, Fighting d6, Intimidation d10,
Notice d12, Spellcasting d8, Stealth d8  
**Pace:** ---; **Parry:** 5; **Toughness:** 5  
**Edges:** Arcane Background (Magic), Alertness  
**Powers:** _Bolt_, _confusion_, _fear_, _stun_; **Power Points:** 20  
**Special Abilities:**

- **Bite:** Str+d4.
- **Flight:** Pace 6.
- **Darkvision:** Spectators ignore penalties for Illumination up to 10" (20
  yards).
- **Multiple Eyes:** The spectator's spells are cast as magical eye rays. When
  casting two different powers on its turn, a spectator may ignore 2 points of
  Multi-Action penalties that turn
- **Spell Reflection:** If a spell targeted at a spectator missed or failed to
  affect it, the spectator can choose another creature within 10" to be targeted
  instead, using the spectator's Spellcasting.
- **Telepathy:** Spectators can freely telepathically speak to creatures within
  20".

## Beasts

### Bat

**Attributes:** Agility d8, Smarts d4 (A), Spirit d6, Strength 1, Vigor d4  
**Skills:** Athletics d8, Fighting d4, Notice d10, Stealth d10  
**Pace:** 1; **Parry:** 4; **Toughness:** 0  
**Edges:** ---  
**Special Abilities:**

- **Bite/Claws:** Str+d4.
- **Blindsense:** Bats ignore invisibility, illusion, and all Illumination
  penalties.
- **Flight:** Pace 24.
- **Size -4 (Tiny):** Bats are flying mice and rarely weigh more than 2 pounds.

### Beaver

**Attributes:** Agility d6, Smarts d4 (A), Spirit d6, Strength d6, Vigor d6  
**Skills:** Athletics d6, Fighting d4, Intimidation d6, Notice d4, Stealth d6,
Survival d6  
**Pace:** 4; **Parry:** 4; **Toughness:** 5  
**Edges:** ---  
**Special Abilities:**

- **Bite:** Str+d4.
- **Semi-Aquatic:** Pace 6.
- **Size -1:** Beavers typically average 3' in length, and weigh up to 70
  pounds.

### Cheetah

**Attributes:** Agility d10, Smarts d6 (A), Spirit d6, Strength d8, Vigor d6  
**Skills:** Athletics d12, Fighting d6, Notice d8, Stealth d8  
**Pace:** 20; **Parry:** 5; **Toughness:** 5  
**Edges:** Quick  
**Special Abilities:**

- **Bite/Claws:** Str+d6.
- **Low Light Vision:** Cheetahs ignore penalties for Dim and Dark Illumination.
- **Pounce:** If a cheetah makes a Wild Attack, it adds +4 to its damage instead
  of +2.
- **Size -1:** Cheetahs are a little taller than most dogs.
- **Speed:** d12 running die.

### Chimpanzee

**Attributes:** Agility d6, Smarts d4 (A), Spirit d6, Strength d8, Vigor d6  
**Skills:** Athletics d10, Fighting d4, Intimidation d6, Notice d6, Persuasion
d4, Stealth d6, Survival d4, Taunt d8  
**Pace:** 6; **Parry:** 4; **Toughness:** 4  
**Edges:** Humiliate, Provoke  
**Special Abilities:**

- **Bite:** Str+d4.
- **Size -1:** Chimpanzees have a standing height between 4' and 5'.

### Dolphin

**Attributes:** Agility d8, Smarts d6 (A), Spirit d6, Strength d8, Vigor d6  
**Skills:** Athletics d10, Fighting d4, Notice d6, Stealth d6  
**Pace:** ---; **Parry:** 4; **Toughness:** 6  
**Edges:** Acrobat  
**Special Abilities:**

- **Bite:** Str+d4.
- **Blindsense:** Dolphins ignore invisibility, illusion, and all Illumination
  penalties.
- **Low Light Vision:** Dolphins ignore penalties for Dim and Dark Illumination.
- **Semi-Aquatic:** Pace 10.
- **Size 1:** Dolphins have an average weight of 500 pounds, and are between 8'
  and 13' long.

### Eel

**Attributes:** Agility d8, Smarts d4 (A), Spirit d6, Strength d6, Vigor d6  
**Skills:** Athletics d6, Fighting d6, Notice d6, Stealth d10  
**Pace:** ---; **Parry:** 6; **Toughness:** 4  
**Edges:** ---  
**Special Abilities:**

- **Aquatic:** Pace 6.
- **Bite:** Str+d4.
- **Burrow (2"):** Eels can burrow through loose sand.
- **Size -1**

### Eel, Electric

**Attributes:** Agility d8, Smarts d4 (A), Spirit d6, Strength d6, Vigor d6  
**Skills:** Athletics d6, Fighting d6, Notice d6, Stealth d10  
**Pace:** ---; **Parry:** 6; **Toughness:** 4  
**Edges:** ---  
**Special Abilities:**

- **Aquatic:** Pace 6.
- **Bite:** Str+d4.
- **Electrolocation:** Electric eels can sense their surroundings within 1"
  without relying on sight.
- **Size -1**
- **Stun:** When an electric eel successfully hits a creature (even if it causes
  no damage), the target must make a Vigor roll or be **Stunned**. If the
  contact occurs outside of water, the target suffers a -2 penalty to the roll.

### Elk

**Attributes:** Agility d8, Smarts d6 (A), Spirit d8, Strength d12, Vigor d10  
**Skills:** Athletics d8, Fighting d6, Notice d8  
**Pace:** 10; **Parry:** 5; **Toughness:** 9  
**Edges:** ---  
**Special Abilities:**

- **Antlers:** Str+d6.
- **Kick:** Str+d4.
- **Size 2:** Elk have thick bodies that weigh well over 500 pounds.
- **Speed:** d8 running die.

### Gorilla

**Attributes:** Agility d6, Smarts d6 (A), Spirit d8, Strength d12, Vigor d8  
**Skills:** Athletics d8, Fighting d8, Intimidation d8, Notice d6, Persuasion
d4, Stealth d4  
**Pace:** 6; **Parry:** 6; **Toughness:** 7  
**Edges:** Menacing  
**Special Abilities:**

- **Bite:** Str+d4.
- **Size 1:** Gorillas are twice as heavy as humans.

### Hippopotamus

**Attributes:** Agility d6, Smarts d4 (A), Spirit d8, Strength d12+4, Vigor
d12  
**Skills:** Athletics d6, Fighting d10, Intimidation d10, Notice d6, Survival
d4  
**Pace:** 6; **Parry:** 7; **Toughness:** 14 (2)  
**Edges:** Frenzy (Imp)  
**Special Abilities:**

- **Armor +2:** Thick skin.
- **Bite:** Str+d8.
- **Fearless:** Hippos are immune to Fear and Intimidation.
- **Low Light Vision:** Hippos ignore penalties for Dim and Dark Illumination.
- **Semi-Aquatic:** Pace 6.
- **Size 4 (Large):** Hippos average 3,000 pounds and stand 5' at the shoulder.

### Killer Whale/Orca

**Attributes:** Agility d6, Smarts d6 (A), Spirit d8, Strength d12+5, Vigor
d12  
**Skills:** Athletics d10, Fighting d10, Notice d6, Stealth d4  
**Pace:** ---; **Parry:** 7; **Toughness:** 16 (2)  
**Edges:** Frenzy (Imp)  
**Special Abilities:**

- **Armor +2:** Thick skin.
- **Bite:** Str+d8.
- **Blindsense:** Killer whales ignore invisibility, illusion, and all
  Illumination penalties.
- **Low Light Vision:** Orcas ignore penalties for Dim and Dark Illumination.
- **Semi-Aquatic:** Pace 10.
- **Size 6 (Large):** Orcas weigh in excess of 6 tons and are 20' to 26' long.
- **Swat:** Killer whales ignore up to 2 points of Scale penalties when biting.

### Octopus

**Attributes:** Agility d6, Smarts d6 (A), Spirit d6, Strength d6, Vigor d8  
**Skills:** Athletics d6, Fighting d4, Notice d6, Stealth d8  
**Pace:** 2; **Parry:** 4; **Toughness:** 6  
**Edges:** Extraction, Quick  
**Special Abilities:**

- **Aquatic:** Pace 6.
- **Bite:** Str+d4.
- **Camouflage:** Octopuses can rapidly change the colour of their skin. When
  stationary, they have a +2 bonus to Stealth rolls.
- **Ink Cloud:** Octopuses can eject a cloud of ink, mirroring the _darkness_
  power (no raise). Creatures that enter the ink cloud must roll Vigor or suffer
  the effects of _blind_ (no raise).
- **Low Light Vision:** Octopuses ignore penalties for Dim and Dark
  Illumination.
- **Poison:** An octopus' bites are venomous. The GM chooses what kind of poison
  the octopus injects (see **Poison** in _Savage Worlds_).
- **Size -1:** Octopuses weigh 32 pounds and upwards.
- **Tentacles:** Str, Reach 1. If the octopus hits, it may immediately make a
  grappling attempt (at +2) as a free action.

### Penguin

**Attributes:** Agility d6, Smarts d4 (A), Spirit d6, Strength d6, Vigor d8  
**Skills:** Athletics d6, Fighting d4, Intimidation d6, Notice d6, Stealth d4  
**Pace:** 5; **Parry:** 4; **Toughness:** 5  
**Edges:** ---  
**Special Abilities:**

- **Bite:** Str+d4.
- **Semi-Aquatic:** Pace 8.
- **Size -1:** Penguins are just short of 4' tall and weigh about 70 pounds.

### Scorpion

**Attributes:** Agility d4, Smarts d4 (A), Spirit d4, Strength d4-3, Vigor d6  
**Skills:** Fighting d6, Notice d4, Stealth d6  
**Pace:** 3; **Parry:** 5; **Toughness:** 1  
**Edges:** ---  
**Special Abilities:**

- **Burrow (2"):** Scorpions can burrow through sand and loose dirt.
- **Claws/Sting:** Str+2.
- **Poison:** The GM chooses what kind of poison the scorpion injects (see
  **Poison** in _Savage Worlds_).
- **Size -4 (Tiny)**

### Sea Horse, Giant

**Attributes:** Agility d8, Smarts d4 (A), Spirit d6, Strength d10, Vigor d8  
**Skills:** Athletics d8, Fighting d4, Notice d6  
**Pace:** ---; **Parry:** 4; **Toughness:** 8  
**Edges:** Fleet-Footed  
**Special Abilities:**

- **Aquatic:** Pace 12.
- **Kick:** Str+d4.
- **Low Light Vision:** Giant sea horses ignore penalties for Dim and Dark
  Illumination.
- **Size 2:** Giant sea horses are tall enough to be able to carry a rider like
  land horses.

### Seal

**Attributes:** Agility d6, Smarts d4 (A), Spirit d6, Strength d6, Vigor d8  
**Skills:** Athletics d8, Fighting d4, Notice d6, Stealth d6  
**Pace:** 2; **Parry:** 4; **Toughness:** 6  
**Edges:** Acrobat  
**Special Abilities:**

- **Bite:** Str+d4.
- **Semi-Aquatic:** Pace 8.

### Snake, Giant Venomous

**Attributes:** Agility d8, Smarts d4 (A), Spirit d6, Strength d6, Vigor d6  
**Skills:** Athletics d6, Fighting d8, Intimidation d6, Notice d12, Stealth d6  
**Pace:** 6; **Parry:** 6; **Toughness:** 5  
**Edges:** Quick  
**Special Abilities:**

- **Bite:** Str+d4.
- **Poison (-2):** The GM chooses what kind of poison the snake injects (see
  **Poison** in _Savage Worlds_).

### Stingray

**Attributes:** Agility d8, Smarts d4 (A), Spirit d6, Strength d8, Vigor d8  
**Skills:** Athletics d8, Fighting d8, Intimidation d4, Notice d6, Stealth d10  
**Pace:** ---; **Parry:** 6; **Toughness:** 6  
**Edges:** ---  
**Special Abilities:**

- **Aquatic:** Pace 10.
- **Poison:** The GM chooses what kind of poison the stingray injects (see
  **Poison** in _Savage Worlds_).
- **Sting:** Str+d8.

### Swan

**Attributes:** Agility d8, Smarts d4 (A), Spirit d6, Strength d8, Vigor d8  
**Skills:** Athletics d6, Fighting d6, Intimidation d8, Notice d6, Stealth d4  
**Pace:** 7; **Parry:** 5; **Toughness:** 5  
**Edges:** Menacing  
**Special Abilities:**

- **Bite/Wing Attack:** Str+d6.
- **Flight:** Pace 24.
- **Size -1:** Swans average 5' tall and typically weigh 25 pounds.

### Tortoise, Giant

**Attributes:** Agility d4, Smarts d4 (A), Spirit d6, Strength d12, Vigor d12  
**Skills:** Athletics d4, Fighting d6, Notice d6, Stealth d4  
**Pace:** 2; **Parry:** 5; **Toughness:** 18 (8)  
**Edges:** ---  
**Special Abilities:**

- **Armor +8:** Thick, nigh-impenetrable shell.
- **Bite:** Str+d6.
- **Retract Into Shell:** As an action, a tortoise can retract into its shell,
  making Called Shots impossible. While retracted, the tortoise can't take
  actions or move. They can take an action to pull out of their shell again.
- **Resilient:** Giant tortoises can take one Wound before they're
  Incapacitated.
- **Size 2:** Giant tortoises are 6 feet long and can weigh up to 1,000 pounds.
- **Speed:** d4 running die.
- **Weakness (Head, Limbs):** Called Shots to a tortoise's head or limbs ignore
  the tortoise's Armor.

### Turtle

**Attributes:** Agility d4, Smarts d4 (A), Spirit d6, Strength d4, Vigor d8  
**Skills:** Athletics d6, Fighting d4, Notice d6, Stealth d6  
**Pace:** 2; **Parry:** 4; **Toughness:** 10 (6)  
**Edges:** ---  
**Special Abilities:**

- **Aquatic:** Pace 6.
- **Armor +6:** Thick defensive shell.
- **Bite:** Str+d4.
- **Retract Into Shell:** As an action, a turtle can retract into its shell,
  making Called Shots impossible. While retracted, the turtle can't take actions
  or move. They can take an action to pull out of their shell again.
- **Size -2 (Small)**
- **Weakness (Head, Limbs):** Called Shots to a turtle's head or limbs ignore
  the turtle's Armor.

### Turtle, Sea

**Attributes:** Agility d4, Smarts d4 (A), Spirit d6, Strength d8, Vigor d8  
**Skills:** Athletics d10, Fighting d6, Notice d6, Stealth d4  
**Pace:** 2; **Parry:** 5; **Toughness:** 12 (6)  
**Edges:** ---  
**Special Abilities:**

- **Aquatic:** Pace 8.
- **Armor +6:** Thick defensive shell.
- **Bite:** Str+d6.
- **Weakness (Head, Limbs):** Called Shots to a sea turtle's head or limbs
  ignore the sea turtle's Armor.

## Constructs

### Monodrone

**Attributes:** Agility d6, Smarts d4, Spirit d6, Strength d4, Vigor d6  
**Skills:** Athletics d4, Fighting d4, Notice d4, Repair d6, Stealth d6  
**Pace:** 6; **Parry:** 4; **Toughness:** 6 (2)  
**Hindrances:** One Eye  
**Edges:** ---  
**Gear:** Dagger (Str+d4), javelin (Range 3/6/12, Damage Str+d4)  
**Special Abilities:**

- **Armor +2:** Metal sheets.
- **Construct:** +2 to recover from being Shaken; ignores 1 point of Wound
  penalties; does not breathe or suffer from disease or poison.
- **Flight:** Pace 6.
- **Size -1:** Monodrones usually don't exceed the height of the average human's
  hip.

### Duodrone

**Attributes:** Agility d6, Smarts d4, Spirit d6, Strength d4, Vigor d6  
**Skills:** Athletics d4, Fighting d4, Notice d6, Repair d6, Stealth d4  
**Pace:** 6; **Parry:** 4; **Toughness:** 6 (2)  
**Edges:** ---  
**Gear:** Javelin (Range 3/6/12, Damage Str+d4)  
**Special Abilities:**

- **Armor +2:** Metal sheets.
- **Construct:** +2 to recover from being Shaken; ignores 1 point of Wound
  penalties; does not breathe or suffer from disease or poison.
- **Size -1:** Duodrones usually don't exceed the height of the average human's
  hip.

### Tridrone

**Attributes:** Agility d6, Smarts d6, Spirit d6, Strength d6, Vigor d6  
**Skills:** Athletics d6, Battle d4, Fighting d6, Notice d6, Persuasion d6,
Repair d6, Stealth d4  
**Pace:** 6; **Parry:** 5; **Toughness:** 6 (2)  
**Edges:** Command  
**Gear:** Javelin (Range 3/6/12, Damage Str+d6)  
**Special Abilities:**

- **Armor +2:** Metal sheets.
- **Construct:** +2 to recover from being Shaken; ignores 1 point of Wound
  penalties; does not breathe or suffer from disease or poison.
- **Size -1:** Tridrones are slightly taller than duodrones, and slightly
  shorter than quadrones.
- **Three Arms:** Tridrones ignore 2 points of Multi-Action penalties each turn.

### Quadrone

**Attributes:** Agility d8, Smarts d6, Spirit d6, Strength d6, Vigor d6  
**Skills:** Athletics d6, Battle d6, Fighting d6, Notice d6, Persuasion d6,
Repair d6, Shooting d8, Stealth d4  
**Pace:** 6; **Parry:** 5; **Toughness:** 8 (4)  
**Edges:** Command Presence, Tactician  
**Gear:** Bow (Range 12/24/48, Damage 2d6)  
**Special Abilities:**

- **Armor +4:** Thick metal sheets.
- **Construct:** +2 to recover from being Shaken; ignores 1 point of Wound
  penalties; does not breathe or suffer from disease or poison.
- **Flight:** Pace 6.
- **Size -1:** Quadrones are 100 pounds worth of cube.

### Pentadrone

**Attributes:** Agility d8, Smarts d6, Spirit d8, Strength d10, Vigor d8  
**Skills:** Athletics d6, Battle d8, Fighting d8, Notice d8, Persuasion d8,
Repair d6, Stealth d4  
**Pace:** 6; **Parry:** 6; **Toughness:** 11 (4)  
**Edges:** Command Presence, Frenzy (Imp), Inspire  
**Special Abilities:**

- **Arm:** Str+d6.
- **Armor +4:** Thick metal sheets.
- **Construct:** +2 to recover from being Shaken; ignores 1 point of Wound
  penalties; does not breathe or suffer from disease or poison.
- **Multiple Arms:** Pentadrones ignore 2 points of Multi-Action penalties each
  turn.
- **Paralysis Gas:** Pentadrones exhale a paralysing gas (see **Breath Weapons**
  in _Savage Worlds_). Those affected must roll Vigor or be Stunned and
  Fatigued.
- **Resilient:** Pentadrones can take one Wound before they're Incapacitated.
- **Size 1:** Pentadrones tower over their inferiors.

## Elementals

### :spades: Dao

**Attributes:** Agility d6, Smarts d8, Spirit d8, Strength d12+3, Vigor d12  
**Skills:** Athletics d8, Common Knowledge d6, Fighting d10, Focus d12,
Intimidation d8, Notice d6, Persuasion d10, Stealth d4, Taunt d8  
**Pace:** 6; **Parry:** 7; **Toughness:** 17 (6)  
**Edges:** Arcane Background (Elementalist), Free Runner, Iron Jaw  
**Gear:** Maul (Str+d10)  
**Powers:** _Barrier_, _burrow_, _detect/conceal arcana_, _entangle_,
_protection_, _speak language_, _summon monster_ (earth elemental, 6 Power
Points), _teleport_; **Power Points:** 25  
**Special Abilities:**

- **Armor +6:** Polished stone skin.
- **Burrow (6"):** Dao can burrow through sand and earth.
- **Darkvision:** Dao ignore penalties for Illumination up to 10" (20 yards).
- **Earth Manipulation:** Dao can move and shape earth and stone to great
  effect---GM's discretion. They can hurl earth or stone at a Large Blast
  Template or Cone Template, doing 3d6 damage, and knocking those who fail a
  Strength -2 roll prone. This attack may be **Evaded** (see _Savage Worlds_).
- **Hardy:** The creature does not suffer a Wound from being Shaken twice.
- **Immunity:** Earth, electricity.
- **Size 3:** Dao average 10' tall.
- **Wish:** Genies can magically grant a mortal's wish. They often twist the
  result of the wish away from its desired intent due to the wish's poor
  wording.

### :spades: Djinni

**Attributes:** Agility d10, Smarts d8, Spirit d10, Strength d12+1, Vigor d12  
**Skills:** Athletics d10, Common Knowledge d6, Fighting d12, Focus d12,
Intimidation d8, Notice d6, Persuasion d10, Stealth d4, Taunt d8  
**Pace:** 6; **Parry:** 8; **Toughness:** 11  
**Edges:** Arcane Background (Elementalist), Dodge (Imp), Extraction (Imp), Free
Runner, Frenzy  
**Gear:** Scimitar (Str+d8)  
**Powers:** _Bolt_, _deflection_, _detect/conceal arcana_, _entangle_,
_environmental protection_, _fly_, _speak language_, _stun_, _summon monster_
(air elemental, 6 Power Points), _teleport_; **Power Points:** 25  
**Special Abilities:**

- **Air Manipulation:** Djinn can manipulate air and lightning to great
  effect---GM's discretion. They can hurl air or lightning at a Large Blast
  Template or Cone Template, doing 3d6 damage, and hurling those who fail a
  Strength -2 roll by 2d6" (air) or making those who fail a Vigor -2 roll
  Stunned (lightning). This attack may be **Evaded** (see _Savage Worlds_).
- **Darkvision:** Djinn ignore penalties for Illumination up to 10" (20 yards).
- **Flight:** Pace 12.
- **Immunity:** Air, electricity, sound-based attacks.
- **Size 3:** Djinn average 10' tall.
- **Unending Breath:** Djinn can hold their breath indefinitely.
- **Wish:** Genies can magically grant a mortal's wish. They often twist the
  result of the wish away from its desired intent due to the wish's poor
  wording.

### :spades: Efreeti

**Attributes:** Agility d8, Smarts d8, Spirit d8, Strength d12+2, Vigor d12  
**Skills:** Athletics d10, Common Knowledge d6, Fighting d12, Focus d12,
Intimidation d10, Notice d46 Persuasion d8, Stealth d4, Taunt d10  
**Pace:** 8; **Parry:** 8; **Toughness:** 11  
**Edges:** Arcane Background (Elementalist), Counterattack (Imp), Frenzy (Imp),
Sweep (Imp)  
**Gear:** Scimitar (Str+d10)  
**Powers:** _Barrier_, _damage field_, _detect/conceal arcana_, _environmental
protection_, _growth/shrink_, _speak language_, _summon monster_ (fire
elemental, 6 Power Points), _teleport_; **Power Points:** 25  
**Special Abilities:**

- **Darkvision:** Efreet ignore penalties for Illumination up to 10" (20 yards).
- **Fire Manipulation:** Efreet can manipulate fire to great effect---GM's
  discretion. They can hurl fire at a Large Blast Template or Cone Template,
  setting fire to those affected, taking 3d6 initial damage. This attack may be
  **Evaded** (see _Savage Worlds_).
- **Flight:** Pace 8.
- **Immunity:** Fire.
- **Size 3:** Efreet average 12' tall.
- **Wish:** Genies can magically grant a mortal's wish. They often twist the
  result of the wish away from its desired intent due to the wish's poor
  wording.

### :spades: Marid

**Attributes:** Agility d6, Smarts d10, Spirit d10, Strength d12+2, Vigor d12  
**Skills:** Athletics d6, Common Knowledge d6, Fighting d8, Focus d12,
Intimidation d8, Notice d6, Persuasion d10, Stealth d4, Taunt d6  
**Pace:** 6; **Parry:** 6 (7 with trident); **Toughness:** 13  
**Edges:** Arcane Background (Elementalist), Iron Jaw, Nerves of Steel (Imp)  
**Gear:** Trident (Str+d8, Parry +1, Reach 1)  
**Powers:** _Darkness_, _detect/conceal arcana_, _environmental protection_,
_entangle_, _speak language_, _summon monster_ (water elemental, 6 Power
Points), _teleport_; **Power Points:** 25  
**Special Abilities:**

- **Aquatic:** Pace 12.
- **Darkvision:** Marids ignore penalties for Illumination up to 10" (20 yards).
- **Hardy:** The creature does not suffer a Wound from being Shaken twice.
- **Immunity:** Water.
- **Size 3:** Marids average 10' tall.
- **Toughness +2:** Marids have a high constitution.
- **Water Manipulation:** Marids can move and shape water to great effect---GM's
  discretion. They can hurl water at a Large Blast Template or Cone Template,
  doing 3d6 damage and hurling those who fail a Strength -2 roll by 2d6". This
  attack may be **Evaded** (see _Savage Worlds_).
- **Wish:** Genies can magically grant a mortal's wish. They often twist the
  result of the wish away from its desired intent due to the wish's poor
  wording.

## Fey

### Boggle

**Attributes:** Agility d10, Smarts d4, Spirit d6, Strength d4, Vigor d8  
**Skills:** Athletics d8, Fighting d4, Notice d10, Persuasion d6, Stealth d10,
Thievery d10  
**Pace:** 6; **Parry:** 4; **Toughness:** 4  
**Edges:** ---  
**Special Abilities:**

- **Darkvision:** Boggles ignore penalties for Illumination up to 10" (20
  yards).
- **Environmental Weakness:** Fire.
- **Oil Puddle:** As an action, the boggle can place down an oil puddle as a
  Small Blast Template centred on themselves. If the oil is slippery, creatures
  entering the template must roll Agility or fall prone. If the oil is sticky,
  those entering the template must roll Strength -2 or Athletics or be
  Entangled.
- **Oily Skin:** A boggle's oily skin is slippery or sticky, and they can freely
  switch between the two modes. With slippery oil, the boggle has a +2 to all
  rolls made to resist grapples. With sticky oil, the boggle has a +2 to all
  rolls made to grapple others, and they can move at their full Pace on walls
  and ceilings.
- **Size -2 (Small):** Boggles stand 2-3' tall.
- **Teleport:** Boggles can freely cast the _teleport_ power without rolling.
- **Uncanny Smell:** Boggles have a +2 to Notice rolls made to smell something.

### Meenlock

**Attributes:** Agility d8, Smarts d6, Spirit d8, Strength d4-1, Vigor d6  
**Skills:** Athletics d6, Fighting d4, Intimidation d10, Notice d8, Stealth d10,
Survival d6  
**Pace:** 6; **Parry:** 4; **Toughness:** 3  
**Edges:** ---  
**Special Abilities:**

- **Claws:** Str+d4.
- **Darkvision:** Meenlocks ignore penalties for Illumination up to 10" (20
  yards).
- **Fear (-2):** Meenlocks project an aura of terror. Anyone who sees a meenlock
  must make a Fear check at -2.
- **Fearless:** Meenlocks are immune to Fear and Intimidation.
- **Shadow Teleport:** Meenlocks can freely cast the _teleport_ power without
  rolling, provided the origin and destination are both dimly lit or darker.
- **Size -2 (Small):** Meenlocks are 2' tall hideous creatures.
- **Telepathy:** Meenlocks can freely telepathically speak to creatures within
  10".
- **Weakness (Sunlight):** The meenlock suffers a -2 penalty to all Trait rolls
  while in sunlight.

### Quickling

**Attributes:** Agility d12, Smarts d6, Spirit d6, Strength d4-1, Vigor d6  
**Skills:** Athletics d12, Fighting d8, Notice d8, Persuasion d4, Stealth d12,
Thievery d12  
**Pace:** 24; **Parry:** 8; **Toughness:** 3  
**Edges:** Block (Imp), Dodge (Imp), Extraction (Imp), Frenzy (Imp), Quick  
**Gear:** Dagger (Str+d4)  
**Special Abilities:**

- **Low Light Vision:** Quicklings ignore penalties for Dim and Dark
  Illumination.
- **Size -2 (Small):** Quicklings are small creatures.
- **Speed:** d12 running die.

## Humanoids

### Agent, Royal Eyes of Aundair

**Attributes:** Agility d8, Smarts d10, Spirit d8, Strength d6, Vigor d6  
**Skills:** Academics d8, Athletics d6, Common Knowledge d6, Fighting d8,
Intimidation d6, Notice d12, Persuasion d8, Research d6, Shooting d8,
Spellcasting d8, Stealth d10, Taunt d6, Thievery d8  
**Pace:** 6; **Parry:** 6; **Toughness:** 8 (3)  
**Edges:** Arcane Background (Magic), Combat Reflexes  
**Gear:** Wand of Bolts, dagger (Range 3/6/12, Str+d4), chain mail (+3)  
**Powers:** _Arcane protection_, _blind_, _detect/conceal arcana_, _disguise_,
_dispel_, _illusion_, _lock/unlock_, _invisibility_, _scrying_, _sound/silence_;
**Power Points:** 15

### Assassin, House Phiarlan/Thuranni

**Attributes:** Agility d12, Smarts d8, Spirit d10, Strength d8, Vigor d6  
**Skills:** Athletics d12, Common Knowledge d6, Fighting d12, Intimidation d8,
Notice d12, Performance d8+1, Persuasion d8, Shooting d12, Stealth d12+1, Taunt
d8, Thievery d10  
**Pace:** 6; **Parry:** 8; **Toughness:** 7 (2)  
**Edges:** Assassin, Combat Reflexes, Dragonmark (Shadow), Extraction (Imp),
Frenzy (Imp), Quick  
**Gear:** Dagger (Str+d4), short sword (Str+d6), light brigandine (+2)  
**Powers:** _Darkness_, _disguise_, _illusion_, _sound/silence_. **Power
Points:** 10  
**Special Abilities:**

- **Fey Ancestry:** Elves get a free reroll when rolling to resist hostile
  enchantment powers and effects, and magic cannot put them to sleep.
- **Low Light Vision:** Elves ignore penalties for Dim and Dark Illumination.

### Deathguard Paladin

**Attributes:** Agility d8, Smarts d6, Spirit d10, Strength d10, Vigor d8-1  
**Skills:** Athletics d8, Battle d6, Common Knowledge d6, Cosmology d6, Faith
d8, Fighting d10, Intimidation d8, Notice d8, Persuasion d8, Riding d8, Stealth
d6  
**Pace:** 6 (5 with shield); **Parry:** 9 (2); **Toughness:** 9 (4)  
**Edges:** Arcane Background (Miracles), Counterattack, Fey Blood, Soldier  
**Gear:** Longsword, (Str+d8), large shield (Parry +2), bronzewood armor (+4)  
**Powers:** _Banish_, _detect/conceal arcana_, _light_, _sanctuary_, _smite_;
**Power Points:** 15  
**Special Abilities:**

- **Frail:** Elves decrease their Toughness by 1 and suffer a -1 penalty to
  Vigor rolls.
- **Low Light Vision:** Elves ignore penalties for Dim and Dark Illumination.

### Gatekeeper Druid, Orc

**Attributes:** Agility d4, Smarts d6, Spirit d10, Strength d8, Vigor d6  
**Skills:** Athletics d6, Common Knowledge d4, Cosmology d8, Faith d6, Fighting
d4, Notice d6, Persuasion d6, Stealth d4, Survival d6  
**Pace:** 6; **Parry:** 4 (5 with staff); **Toughness:** 7 (1)  
**Edges:** Arcane Background (Druid), Strong Willed  
**Gear:** Battle axe (Str+d8), staff (Str+d4, Parry +1, Reach 1), light leather
armor (+1)  
**Powers:** _Banish_, _beast friend_, _dispel_, _havoc_, _healing_, _shape
change_, _smite_; **Power Points:** 15  
**Special Abilities:**

- **Darkvision:** Orcs ignore penalties for Illumination up to 10" (20 yards).
- **Size 1:** Orcs are hulking creatures.

### :spades: Kuo-toa Archpriest

**Attributes:** Agility d6, Smarts d8, Spirit d10, Strength d6, Vigor d6  
**Skills:** Academics d8, Athletics d6, Common Knowledge d6, Faith d10, Fighting
d4, Notice d10, Persuasion d8, Stealth d4  
**Pace:** 6; **Parry:** 4 (5 with sceptre); **Toughness:** 6  
**Edges:** Arcane Background (Miracles)  
**Gear:** Sceptre (Str+d4, Parry +1, Reach 1)  
**Powers:** _Banish_, _bolt_, _detect/conceal arcana_, _divination_, _havoc_,
_healing_, _protection_, _scrying_, _speak language_, _stun_; **Power Points:**
25  
**Special Abilities:**

- **Aquatic:** Pace 6.
- **Darkvision:** Kuo-toa ignore penalties for Illumination up to 10" (20
  yards).
- **Otherworldly Perception:** Kuo-toa can sense the presence of any moving
  creature within 10" (20 yards) that is invisible or ethereal. They gain a +2
  bonus to rolls to attack these creatures.
- **Slippery:** Kuo-toa have a +2 bonus to rolls made to resist being grappled
  and rolls made to break free from being Bound or Entangled.
- **Sunlight Sensitivity:** Kobolds are Distracted in sunlight.
- **Toughness +1:** Kuo-toa have a scaly skin.

### Sahuagin

**Attributes:** Agility d6, Smarts d6, Spirit d6, Strength d8, Vigor d6  
**Skills:** Athletics d10, Common Knowledge d4, Fighting d8, Intimidation d8,
Notice d8, Persuasion d4, Stealth d4, Survival d6  
**Pace:** 6; **Parry:** 6 (7 with spear); **Toughness:** 6  
**Edges:** Frenzy  
**Gear:** Spear (Str+d6, Parry +1, Reach 1)  
**Special Abilities:**

- **Aquatic:** Pace 6.
- **Bite/Claws:** Str+d4.
- **Blood Frenzy:** When a sahuagin makes a Wild Attack against a bleeding
  creature, they add +4 to their damage instead of +2.
- **Darkvision:** Sahuagin ignore penalties for Illumination up to 10" (20
  yards).
- **Toughness +1:** Sahuagin have a scaly skin.
- **Water Dependency:** Sahuagin must immerse themselves in water one hour out
  of every 24 or become automatically Fatigued each day until they are
  Incapacitated. The day after Incapacitation from dehydration, they perish.
  Each hour spent in water restores one level of Fatigue.

### :spades: Sahuagin Baron

**Attributes:** Agility d8, Smarts d6, Spirit d8, Strength d10, Vigor d10  
**Skills:** Athletics d10, Common Knowledge d4, Fighting d10, Intimidation d10,
Notice d8, Persuasion d6, Stealth d4, Survival d6  
**Pace:** 6; **Parry:** 7 (8 with trident); **Toughness:** 11 (3)  
**Edges:** Aristocrat, Command, Fervor, Frenzy  
**Gear:** Trident (Str+d6, Parry +1, Reach 1), carapace armor (+3)  
**Special Abilities:**

- **Aquatic:** Pace 6.
- **Bite/Claws:** Str+d4.
- **Blood Frenzy:** When a sahuagin makes a Wild Attack against a bleeding
  creature, they add +4 to their damage instead of +2.
- **Darkvision:** Sahuagin ignore penalties for Illumination up to 10" (20
  yards).
- **Four Arms:** Sahuagin barons ignore 2 points of Multi-Action penalties each
  turn.
- **Toughness +1:** Sahuagin have a scaly skin.
- **Water Dependency:** Sahuagin must immerse themselves in water one hour out
  of every 24 or become automatically Fatigued each day until they are
  Incapacitated. The day after Incapacitation from dehydration, they perish.
  Each hour spent in water restores one level of Fatigue.

### Sahuagin Priestess

**Attributes:** Agility d6, Smarts d6, Spirit d8, Strength d8, Vigor d6  
**Skills:** Academics d6, Athletics d8, Common Knowledge d6, Cosmology d6, Faith
d8, Fighting d6, Intimidation d8, Notice d8, Persuasion d6, Stealth d4, Survival
d6  
**Pace:** 6; **Parry:** 5 (6 with spear); **Toughness:** 6  
**Edges:** Arcane Background (Miracles), Frenzy  
**Gear:** Spear (Str+d6, Parry +1, Reach 1)  
**Powers:** _Bolt_, _beast friend_, _boost/lower trait_, _smite_, _stun_;
**Power Points:** 15  
**Special Abilities:**

- **Aquatic:** Pace 6.
- **Bite/Claws:** Str+d4.
- **Blood Frenzy:** When a sahuagin makes a Wild Attack against a bleeding
  creature, they add +4 to their damage instead of +2.
- **Darkvision:** Sahuagin ignore penalties for Illumination up to 10" (20
  yards).
- **Toughness +1:** Sahuagin have a scaly skin.
- **Water Dependency:** Sahuagin must immerse themselves in water one hour out
  of every 24 or become automatically Fatigued each day until they are
  Incapacitated. The day after Incapacitation from dehydration, they perish.
  Each hour spent in water restores one level of Fatigue.

## Monstrosities

### Bulette

**Attributes:** Agility d6, Smarts d4 (A), Spirit d6, Strength d12+1, Vigor
d12  
**Skills:** Athletics d10, Fighting d10, Intimidation d8, Notice d8, Stealth
d6  
**Pace:** 8; **Parry:** 7; **Toughness:** 12  
**Edges:** ---  
**Special Abilities:**

- **Bite:** Str+d6.
- **Burrow (8"):** Bulettes can burrow through sand and ground.
- **Darkvision:** Bulettes ignore penalties for Illumination up to 10" (20
  yards).
- **Deadly Leap:** If a bulette jumps at least 3" on its turn, it can use an
  action to crash down onto the ground. All creatures in a Medium Blast Template
  at the landing site take 3d6 damage and are knocked prone. This attack can be
  **Evaded**.
- **Size 4 (Large):** Bulettes are land sharks that are 12 feet long.
- **Standing Leap:** A bulette can jump 6" horizontally and 3" vertically, with
  or without a running start.
- **Tremorsense:** Bulettes can detect and pinpoint the origin of vibrations
  within 12".

### Displacer Beast

**Attributes:** Agility d10, Smarts d4 (A), Spirit d6, Strength d12, Vigor d10  
**Skills:** Athletics d10, Fighting d8, Intimidation d6, Notice d8, Stealth
d10  
**Pace:** 8; **Parry:** 6; **Toughness:** 8  
**Edges:** Dodge (Imp), Frenzy  
**Special Abilities:**

- **Bite/Claws/Tentacles:** Str+d6.
- **Displacement:** When struck by an attack, the displacer beast rolls Agility.
  On a success, the attack misses.
- **Low Light Vision:** Displacer beasts ignore penalties for Dim and Dark
  Illumination.
- **Size 1:** Displacer beasts are lion-sized six-legged predators.

### Ettercap

**Attributes:** Agility d8, Smarts d4, Spirit d6, Strength d8, Vigor d6  
**Skills:** Athletics d8, Fighting d6, Notice d8, Shooting d6, Stealth d8,
Survival d6  
**Pace:** 6; **Parry:** 5; **Toughness:** 5  
**Edges:** ---  
**Special Abilities:**

- **Bite:** Str+d4.
- **Claws:** Str+d6.
- **Darkvision:** Ettercaps ignore penalties for Illumination up to 10" (20
  yards).
- **Poison (-2):** The GM chooses what kind of poison the ettercap injects (see
  **Poison** in _Savage Worlds_).
- **Wall Walker:** Ettercaps move at their full Pace on walls and ceilings.
- **Webbing:** Ettercaps can cast webs from their thorax that are the size of
  Small Blast Templates. This is a Shooting roll with a Range of 6". A hit means
  the victim is Entangled, or Bound with a raise (see **Bound & Entangled** in
  _Savage Worlds_).

### Gorgon

**Attributes:** Agility d6, Smarts d4 (A), Spirit d8, Strength d12+4, Vigor
d12  
**Skills:** Athletics d8, Fighting d10, Intimidation d10, Notice d6  
**Pace:** 7; **Parry:** 7; **Toughness:** 14 (2)  
**Edges:** Charge, Frenzy  
**Special Abilities:**

- **Armor +2:** Metal plating.
- **Darkvision:** Gorgons ignore penalties for Illumination up to 10" (20
  yards).
- **Horns:** Str+d6, AP 2.
- **Kick:** Str+d4.
- **Petrifying Breath:** As a limited action, the gorgon exhales a petrifying
  gas using a Cone Template. All affected creatures must roll Vigor or be
  Stunned. If the victim fails their roll to recover from being Stunned, they
  turn to stone. Victims may be restored with _dispel_ at -2, but each caster
  may only try once.
- **Size 4 (Large):** Gorgons are the size of bulls and weigh 2 tons.

### Merrow

**Attributes:** Agility d6, Smarts d4, Spirit d6, Strength d10, Vigor d8  
**Skills:** Athletics d10, Common Knowledge d4, Fighting d8, Intimidation d8,
Notice d6, Persuasion d4, Stealth d6  
**Pace:** 2; **Parry:** 7; **Toughness:** 8  
**Edges:** Frenzy  
**Gear:** Harpoon (Range 3/6/12, Damage Str+d6, Reach 1, Parry +1)  
**Special Abilities:**

- **Aquatic:** Pace 8.
- **Bite/Claws:** Str+d6.
- **Low Light Vision:** Merrow ignore penalties for Dim and Dark Illumination.
- **Size 2:** Merrow are between 8' and 9' tall.

## Undead

### :spades: Vampire

**Attributes:** Agility d10, Smarts d10, Spirit d8, Strength d10, Vigor d10  
**Skills:** Athletics d8, Common Knowledge d10, Fighting d10, Intimidation d12,
Notice d8, Persuasion d12, Stealth d10  
**Pace:** 6; **Parry:** 7; **Toughness:** 9  
**Edges:** Frenzy (Imp), Iron Will  
**Special Abilities:**

- **Bite/Claws:** Str+d4.
- **Blood Drain:** Whenever a vampire bites an opponent, they cause Energy Drain
  (Vigor), which the victim resists using Vigor (at -2 if bound). The vampire
  recovers 1 Wound when the Energy Drain is successful. If the victim dies in
  this way, they are raised as a vampire spawn (or a vampire if a Wild Card) the
  night after their burial.
- **Charm:** Vampires can cast _puppet_ using Smarts without expending Power
  Points, and can maintain the power indefinitely. Wild Cards may roll Spirit to
  recover from being charmed every 24 hours.
- **Darkvision:** Vampires ignore penalties for Illumination up to 10" (20
  yards).
- **Fast Regeneration:** Vampires may attempt a natural healing roll every round
  until Incapacitated.
- **Misty Escape:** If Incapacitated, the vampire transforms into a cloud of
  mist and attempts to escape. It must reach its coffin home within 2 hours or
  be utterly destroyed. Once at rest, the vampire is helpless. It removes one
  Wound after one hour, and is no longer Incapacitated.
- **Shapechanger:** Vampires can cast _shape change_ without expending Power
  Points to turn into a bat or a cloud of mist. The power automatically succeeds
  and can be maintained indefinitely. The cloud of mist is immune to everything
  except running water and sunlight and flies at Pace 5.
- **Undead:** +2 Toughness; +2 to recover from being Shaken; no additional
  damage from Called Shots; ignores 1 point of Wound penalties; doesn't breathe;
  immune to disease and poison.
- **Weakness (Garlic):** Vampires cannot tolerate the strong odor of garlic and
  will not enter an area laced with it.
- **Weakness (Holy Symbol):** A character may keep a vampire at bay by
  displaying a holy symbol. A vampire who wants to directly attack the victim
  must beat them in an opposed Spirit roll.
- **Weakness (Invitation Only):** Vampires cannot enter a private dwelling
  without being invited. They may enter public domains as they please.
- **Weakness (Running Water and Sunlight):** Each round of immersion in running
  water or direct sunlight, the vampire must make a Vigor roll or suffer one
  Wound---a vampire Incapacitated in this manner is destroyed.
- **Weakness (Stake Through the Heart):** Driving a wooden stake through a
  vampire's heart instantly slays it. However, it returns to life if the stake
  is removed.

### Vampire Spawn

**Attributes:** Agility d10, Smarts d6, Spirit d6, Strength d8, Vigor d8  
**Skills:** Athletics d8, Common Knowledge d6, Fighting d8, Intimidation d8,
Notice d8, Persuasion d4, Stealth d8  
**Pace:** 6; **Parry:** 6; **Toughness:** 8  
**Edges:** Frenzy (Imp)  
**Special Abilities:**

- **Bite/Claws:** Str+d4.
- **Blood Drain:** Whenever a vampire spawn bites an opponent, they cause Energy
  Drain (Vigor), which the victim resists using Vigor (at -2 if bound). The
  vampire spawn recovers 1 Wound when the Energy Drain is successful.
- **Darkvision:** Vampire spawn ignore penalties for Illumination up to 10" (20
  yards).
- **Fast Regeneration:** Vampire spawn automatically recover from being Shaken
  at the start of their turn.
- **Undead:** +2 Toughness; +2 to recover from being Shaken; no additional
  damage from Called Shots; ignores 1 point of Wound penalties; doesn't breathe;
  immune to disease and poison.
- **Weakness (Garlic):** Vampire spawn cannot tolerate the strong odor of garlic
  and will not enter an area laced with it.
- **Weakness (Holy Symbol):** A character may keep a vampire spawn at bay by
  displaying a holy symbol. A vampire spawn who wants to directly attack the
  victim must beat them in an opposed Spirit roll.
- **Weakness (Invitation Only):** Vampire spawn cannot enter a private dwelling
  without being invited. They may enter public domains as they please.
- **Weakness (Running Water and Sunlight):** Each round of immersion in running
  water or direct sunlight, the vampire spawn must make a Vigor roll or suffer
  one Wound---a vampire spawn Incapacitated in this manner is destroyed.
- **Weakness (Stake Through the Heart):** Driving a wooden stake through a
  vampire spawn's heart instantly slays it.
