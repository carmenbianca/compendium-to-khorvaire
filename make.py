# SPDX-FileCopyrightText: © 2020 Carmen Bianca Bakker <carmen@carmenbianca.eu>
#
# SPDX-License-Identifier: CC-BY-4.0

from pathlib import Path
import os
import subprocess

from jinja2 import Environment, FileSystemLoader


def get_latest_revision(filename):
    return (
        subprocess.run(
            ["git", "log", "-n", "1", "--pretty=format:%as", filename],
            stdout=subprocess.PIPE,
        )
        .stdout.decode("utf-8")
        .strip()
    )


def create_variables(filename=None, html_out=None, title=None, date=None):
    return {
        "filename": filename,
        "html_out": html_out,
        "title": title,
        "date": get_latest_revision(filename),
    }


def combine(template, variables, document):
    header = template.render(variables)
    out = header
    out += "\n\n"
    out += document
    return out.encode("utf-8")


def html(template, variables):
    target = f"public/{variables['html_out']}"
    print(f"Converting {variables['filename']} to {target}")
    document = Path(variables["filename"]).read_text()
    subprocess.run(
        [
            "pandoc",
            "-f",
            "markdown+emoji",
            "-s",
            "--toc",
            "-c",
            "style.css",
            "-c",
            "../style.css",
            "--template",
            "template.html",
            "-o",
            target,
        ],
        input=combine(template, variables, document),
        check=True,
    )


def epub(template, variables):
    target = f"public/{variables['title']}.epub"
    print(f"Converting {variables['filename']} to {target}")
    document = Path(variables["filename"]).read_text()
    subprocess.run(
        ["pandoc", "-f", "markdown+emoji", "-o", target],
        input=combine(template, variables, document),
        check=True,
    )


def pdf(template, variables):
    variables["latex"] = True
    target = f"public/{variables['title']}.pdf"
    print(f"Converting {variables['filename']} to {target}")
    document = Path(variables["filename"]).read_text()
    subprocess.run(
        ["pandoc", "-f", "markdown+emoji", "--pdf-engine=xelatex", "-o", target],
        input=combine(template, variables, document),
        check=True,
    )


def onecolumn_pdf(template, variables):
    variables["latex"] = True
    variables["onecolumn"] = True
    target = f"public/{variables['title']}.onecolumn.pdf"
    print(f"Converting {variables['filename']} to {target}")
    document = Path(variables["filename"]).read_text()
    document = document.replace("\\onecolumn", "")
    document = document.replace("\\twocolumn", "")
    subprocess.run(
        ["pandoc", "-f", "markdown+emoji", "--pdf-engine=xelatex", "-o", target],
        input=combine(template, variables, document),
        check=True,
    )


if __name__ == "__main__":
    dir_path = os.path.dirname(os.path.realpath(__file__))

    env = Environment(loader=FileSystemLoader(dir_path))
    template = env.get_template("template.jinja2")

    files = [
        create_variables(
            filename="compendium-to-khorvaire.md",
            html_out="index.html",
            title="Wild Cards' Compendium to Khorvaire",
        ),
        create_variables(
            filename="expanded-compendium-to-khorvaire.md",
            html_out="expanded.html",
            title="Wild Cards' Expanded Compendium to Khorvaire",
        ),
    ]

    for file_ in files:
        html(template, file_.copy())
        epub(template, file_.copy())
        pdf(template, file_.copy())
        # onecolumn_pdf(template, file_.copy())
